﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;

namespace AdventOfCode2020.Day12
{
    class Solver : ISolver
    {
        public int DayNumber => 12;

        public string FirstExpected => "2458";

        public string SecondExpected => "145117";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "F10",
                    "N3",
                    "F7",
                    "R90",
                    "F11",
                }, new Vector(17, 8))
                .AddTestCase(new[]
                {
                    "F10",
                    "N3",
                    "F7",
                    "L90",
                    "F11",
                }, new Vector(17, -14))
                .AddTestCase(new[]
                {
                    "F10",
                    "N3",
                    "F7",
                    "R180",
                    "F11",
                }, new Vector(6, -3))
                .AddTest(MoveBasedOnDirection)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "F10",
                    "N3",
                    "F7",
                    "R90",
                    "F11",
                }, "286")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var positionVector = MoveBasedOnDirection(input);
            return positionVector.Length.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var positionVector = MoveBasedOnWaypoint(input);
            return positionVector.Length.ToString();
        }

        private Vector MoveBasedOnDirection(IEnumerable<string> input)
        {
            var position = new Vector(0, 0);
            var direction = new Vector(1, 0);

            foreach (var line in input)
            {
                var action = line[0];
                var value = int.Parse(line.Substring(1));

                switch (action)
                {
                    case 'N':
                        position.Add(Vectors.NorthWith(value));
                        break;
                    case 'S':
                        position.Add(Vectors.SouthWith(value));
                        break;
                    case 'E':
                        position.Add(Vectors.EastWith(value));
                        break;
                    case 'W':
                        position.Add(Vectors.WestWith(value));
                        break;
                    case 'L':
                        direction.RotateLeft(value);
                        break;
                    case 'R':
                        direction.RotateRight(value);
                        break;
                    case 'F':
                        var moveVector = new Vector(direction);
                        moveVector.Multiply(value);
                        position.Add(moveVector);
                        break;
                    default:
                        throw new InvalidOperationException("Invalid action");
                }
            }

            return position;
        }

        private Vector MoveBasedOnWaypoint(IEnumerable<string> input)
        {
            var position = new Vector(0, 0);
            var waypoint = new Vector(10, -1);

            foreach (var line in input)
            {
                var action = line[0];
                var value = int.Parse(line.Substring(1));

                switch (action)
                {
                    case 'N':
                        waypoint.Add(Vectors.NorthWith(value));
                        break;
                    case 'S':
                        waypoint.Add(Vectors.SouthWith(value));
                        break;
                    case 'E':
                        waypoint.Add(Vectors.EastWith(value));
                        break;
                    case 'W':
                        waypoint.Add(Vectors.WestWith(value));
                        break;
                    case 'L':
                        waypoint.RotateLeft(value);
                        break;
                    case 'R':
                        waypoint.RotateRight(value);
                        break;
                    case 'F':
                        var moveVector = new Vector(waypoint);
                        moveVector.Multiply(value);
                        position.Add(moveVector);
                        break;
                    default:
                        throw new InvalidOperationException("Invalid action");
                }
            }

            return position;
        }
    }
}
