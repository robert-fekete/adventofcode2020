﻿namespace AdventOfCode2020.Day12
{
    class Vectors
    {
        private static readonly Vector NORTH = new Vector(0, -1);
        private static readonly Vector EAST = new Vector(1, 0);
        private static readonly Vector SOUTH = new Vector(0, 1);
        private static readonly Vector WEST = new Vector(-1, 0);

        public static Vector NorthWith(int scale)
        {
            return ScaleWith(NORTH, scale);
        }

        public static Vector EastWith(int scale)
        {
            return ScaleWith(EAST, scale);
        }

        public static Vector SouthWith(int scale)
        {
            return ScaleWith(SOUTH, scale);
        }

        public static Vector WestWith(int scale)
        {
            return ScaleWith(WEST, scale);
        }

        private static Vector ScaleWith(Vector vector, int scale)
        {
            var newVector = new Vector(vector);
            newVector.Multiply(scale);

            return newVector;
        }
    }
}
