﻿using System;

namespace AdventOfCode2020.Day12
{
    class Vector
    {
        private int x;
        private int y;

        public Vector(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public Vector(Vector other)
        {
            x = other.x;
            y = other.y;
        }

        public int Length => Math.Abs(x) + Math.Abs(y);

        public void Add(Vector other)
        {
            x += other.x;
            y += other.y;
        }

        public void Multiply(int scale)
        {
            x *= scale;
            y *= scale;
        }

        public void RotateLeft(int angle)
        {
            for (int degrees = angle; degrees > 0; degrees -= 90)
            {
                var newX = y;
                var newY = -1 * x;
                (x, y) = (newX, newY);
            }
        }

        public void RotateRight(int angle)
        {
            for (int degrees = angle; degrees > 0; degrees -= 90)
            {
                var newX = -1 * y;
                var newY = x;
                (x, y) = (newX, newY);
            }
        }

        public override bool Equals(object? obj)
        {
            return obj is Vector vector &&
                   x == vector.x &&
                   y == vector.y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(x, y);
        }

        public override string ToString()
        {
            return $"X: {x}, Y:{y}";
        }
    }
}
