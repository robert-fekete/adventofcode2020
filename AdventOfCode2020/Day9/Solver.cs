﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day9
{
    class Solver : ISolver
    {
        public int DayNumber => 9;

        public string FirstExpected => "248131121";

        public string SecondExpected => "31580383";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    35ul,
                    20ul,
                    15ul,
                    25ul,
                    47ul,
                    40ul,
                    62ul,
                    55ul,
                    65ul,
                    95ul,
                    102ul,
                    117ul,
                    150ul,
                    182ul,
                    127ul,
                    219ul,
                    299ul,
                    277ul,
                    309ul,
                    576ul,
                }
                , 5, 127ul)
                .AddTest(FindFirstMistake)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    35ul,
                    20ul,
                    15ul,
                    25ul,
                    47ul,
                    40ul,
                    62ul,
                    55ul,
                    65ul,
                    95ul,
                    102ul,
                    117ul,
                    150ul,
                    182ul,
                    127ul,
                    219ul,
                    299ul,
                    277ul,
                    309ul,
                    576ul,
                }
                , 127ul, 62ul)
                .AddTest(FindWeakness)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var numbers = input.Select(ulong.Parse).ToArray();

            return FindFirstMistake(numbers, 25).ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var numbers = input.Select(ulong.Parse).ToArray();
            var mistake = FindFirstMistake(numbers, 25);
            var weakness = FindWeakness(numbers, mistake);

            return weakness.ToString();
        }

        private ulong FindFirstMistake(ulong[] message, int preambleSize)
        {
            var queue = new Queue<ulong>();
            for (int i = 0; i < preambleSize; i++)
            {
                queue.Enqueue(message[i]);
            }

            for (int i = preambleSize; i < message.Length; i++)
            {
                if (!ValidateNumber(queue, message[i]))
                {
                    return message[i];
                }
                queue.Enqueue(message[i]);
                queue.Dequeue();
            }

            throw new InvalidOperationException("There was no mistake");
        }

        private bool ValidateNumber(IReadOnlyCollection<ulong> preamble, ulong target)
        {
            var seen = new HashSet<ulong>();
            foreach (var number in preamble)
            {
                if (seen.Contains(number))
                {
                    return true;
                }

                seen.Add(target - number);
            }

            return false;
        }

        private ulong FindWeakness(ulong[] numbers, ulong mistake)
        {
            var rangeSum = new RangeSum(numbers);
            var range = rangeSum.FindRange(mistake);
            return range.Min() + range.Max();
        }
    }
}
