﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day9
{
    class RangeSum
    {
        private readonly List<ulong> sumList = new List<ulong>();
        private readonly ulong[] numbers;

        public RangeSum(ulong[] numbers)
        {
            PrepareSumList(numbers);
            this.numbers = numbers;
        }

        public IEnumerable<ulong> FindRange(ulong target)
        {
            var from = 0;
            var to = 0;
            while (from < sumList.Count && to < sumList.Count)
            {
                var sum = sumList[to + 1] - sumList[from];
                if (sum == target)
                {
                    return numbers.Skip(from).Take(to - from + 1);
                }
                else if (sum < target)
                {
                    to++;
                }
                else
                {
                    from++;
                }
            }

            throw new InvalidOperationException("Couldn't find a matching range");
        }

        private void PrepareSumList(ulong[] numbers)
        {
            sumList.Add(0);
            var last = 0ul;
            foreach(var number in numbers)
            {
                last += number;
                sumList.Add(last);
            }
        }
    }
}
