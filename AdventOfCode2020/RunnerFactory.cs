﻿using AdventOfCode.Framework;
using System.Collections.Generic;

namespace AdventOfCode2020
{
    public class RunnerFactory
    {
        private readonly List<ISolver> solvers = new List<ISolver>
        {
            new Day1.Solver(),
            new Day2.Solver(),
            new Day3.Solver(),
            new Day4.Solver(),
            new Day5.Solver(),
            new Day6.Solver(),
            new Day7.Solver(),
            new Day8.Solver(),
            new Day9.Solver(),
            new Day10.Solver(),
            new Day11.Solver(),
            new Day12.Solver(),
            new Day13.Solver(),
            new Day14.Solver(),
            new Day15.Solver(),
            new Day16.Solver(),
            new Day17.Solver(),
            new Day18.Solver(),
            new Day19.Solver(),
            new Day20.Solver(),
            new Day21.Solver(),
            new Day22.Solver(),
            new Day23.Solver(),
            new Day24.Solver(),
            new Day25.Solver(),
        };

        public IRunner Create()
        {
            return new Runner(solvers);
        }
    }
}
