﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day1
{
    class Solver : ISolver
    {
        public int DayNumber => 1;

        public string FirstExpected => "713184";

        public string SecondExpected => "261244452";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "1721",
                    "979",
                    "366",
                    "299",
                    "675",
                    "1456"
                }, "514579")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "1721",
                    "979",
                    "366",
                    "299",
                    "675",
                    "1456"
                }, "241861950")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var numbers = input.Select(int.Parse);
            
            var seen = new HashSet<int>();
            foreach(var number in numbers)
            {
                if (seen.Contains(number))
                {
                    return (number * (2020 - number)).ToString();
                }

                seen.Add(2020 - number);
            }

            throw new InvalidOperationException("Couldn't find a pair");
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var numbers = input.Select(int.Parse);

            foreach (var number1 in numbers)
            {
                foreach (var number2 in numbers)
                {
                    foreach (var number3 in numbers)
                    {
                        if (number1 + number2 + number3 == 2020)
                        {
                            return ((long)number1 * number2 * number3).ToString();
                        }
                    }
                }
            }

            throw new InvalidOperationException("Couldn't find a pair");
        }
    }
}
