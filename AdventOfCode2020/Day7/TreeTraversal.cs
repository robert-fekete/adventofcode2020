﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day7
{
    class TreeTraversal
    {
        public int CountAncestors(Dictionary<string, IReadOnlyCollection<string>> graph, string startingNode)
        {
            var counter = 0;
            var visited = new HashSet<string>();
            var backlog = new Queue<string>();
            backlog.Enqueue(startingNode);

            while (backlog.Any())
            {
                var currentNode = backlog.Dequeue();

                if (visited.Contains(currentNode))
                {
                    continue;
                }
                visited.Add(currentNode);

                counter++;

                if (!graph.ContainsKey(currentNode))
                {
                    continue;
                }

                foreach(var nextNode in graph[currentNode])
                {
                    backlog.Enqueue(nextNode);
                }
            }

            return counter - 1; // -1 for the starting node
        }

        public long CountChildren(Dictionary<string, IReadOnlyCollection<(int, string)>> graph, string startingNode)
        {
            var counter = 0L;
            var backlog = new Stack<(long, string)>();
            backlog.Push((1, startingNode));

            while (backlog.Any())
            {
                (var number, var currentNode) = backlog.Pop();

                counter += number;

                if (!graph.ContainsKey(currentNode))
                {
                    continue;
                }

                foreach ((var nextNumber, var nextNode) in graph[currentNode])
                {
                    backlog.Push((number * nextNumber, nextNode));
                }
            }

            return counter - 1; // -1 for the starting node
        }
    }
}
