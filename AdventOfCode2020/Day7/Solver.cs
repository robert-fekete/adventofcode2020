﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day7
{
    class Solver : ISolver
    {
        public int DayNumber => 7;

        public string FirstExpected => "335";

        public string SecondExpected => "2431";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "light red bags contain 1 bright white bag, 2 muted yellow bags.",
                    "dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
                    "bright white bags contain 1 shiny gold bag.",
                    "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
                    "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
                    "dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
                    "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
                    "faded blue bags contain no other bags.",
                    "dotted black bags contain no other bags.",
                }, "4")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "light red bags contain 1 bright white bag, 2 muted yellow bags.",
                    "dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
                    "bright white bags contain 1 shiny gold bag.",
                    "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
                    "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
                    "dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
                    "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
                    "faded blue bags contain no other bags.",
                    "dotted black bags contain no other bags.",
                }, "32")
                .AddTestCase(new[]
                {
                    "shiny gold bags contain 2 dark red bags.",
                    "dark red bags contain 2 dark orange bags.",
                    "dark orange bags contain 2 dark yellow bags.",
                    "dark yellow bags contain 2 dark green bags.",
                    "dark green bags contain 2 dark blue bags.",
                    "dark blue bags contain 2 dark violet bags.",
                    "dark violet bags contain no other bags.",
                }, "126")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var graph = ParseGraph<string>(input, ParseBagForAncestor);
            var ancestorCalculator = new TreeTraversal();
            var numberOfAncestors = ancestorCalculator.CountAncestors(graph, "shiny gold");

            return numberOfAncestors.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var graph = ParseGraph<(int, string)>(input, ParseBagForChildren);
            var ancestorCalculator = new TreeTraversal();
            var numberOfChildren = ancestorCalculator.CountChildren(graph, "shiny gold");

            return numberOfChildren.ToString();
        }

        private Dictionary<string, IReadOnlyCollection<T>> ParseGraph<T>(IEnumerable<string> input, Action<Dictionary<string, List<T>>, string, string> parser)
        {
            var graph = new Dictionary<string, List<T>>();
            foreach (var line in input)
            {
                var mainParts = line.Split("contain");
                var outerBag = mainParts[0].Split(" bags")[0];
                if (mainParts[1].Contains("no other"))
                {
                    continue;
                }

                if (mainParts[1].Contains(","))
                {
                    var innerBags = mainParts[1].Split(", ");
                    foreach (var bag in innerBags)
                    {
                        parser(graph, bag, outerBag);
                    }
                }
                else
                {
                    parser(graph, mainParts[1], outerBag);
                }

            }

            return graph.ToDictionary<KeyValuePair<string, List<T>>, string, IReadOnlyCollection<T>>(kvp => kvp.Key, kvp => kvp.Value);
        }

        private void ParseBagForAncestor(Dictionary<string, List<string>> graph, string token, string outerBag)
        {
            var numberAndColor = token.Split("bag")[0].Trim();
            var index = numberAndColor.IndexOf(' ');

            var innerBag = numberAndColor.Substring(index).Trim();

            AddBag(graph, innerBag, outerBag);
        }

        private void ParseBagForChildren(Dictionary<string, List<(int, string)>> graph, string token, string outerBag)
        {
            var numberAndColor = token.Split("bag")[0].Trim();
            var index = numberAndColor.IndexOf(' ');

            var number = int.Parse(numberAndColor.Substring(0, index));
            var innerBag = numberAndColor.Substring(index).Trim();

            AddBag(graph, outerBag, (number, innerBag));
        }

        private static void AddBag<T>(Dictionary<string, List<T>> graph, string key, T value)
        {
            if (!graph.ContainsKey(key))
            {
                graph[key] = new List<T>();
            }

            graph[key].Add(value);
        }
    }
}
