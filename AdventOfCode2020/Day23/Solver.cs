﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace AdventOfCode2020.Day23
{
    class Solver : ISolver
    {
        public int DayNumber => 23;

        public string FirstExpected => "97632548";

        public string SecondExpected => "";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase("389125467", 10, "92658374")
                .AddTestCase("389125467", 100, "67384529")
                .AddTest(PlayCrabCup)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                //.AddTestCase("389125467", 10000000, 149245887792L)
                //.AddTest(PlayAdvancedCrabCup)
                .Build(true);
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return PlayCrabCup(input.First(), 100);
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return PlayAdvancedCrabCup(input.First(), 10000000).ToString();
        }

        private string PlayCrabCup(string initialState, int numberOfTurns)
        {
            var crabCup = new CrabCup(initialState);
            for(int i = 0; i < numberOfTurns; i++)
            {
                crabCup.Move();
            }

            return crabCup.GetSignature();
        }

        private long PlayAdvancedCrabCup(string initialState, int numberOfTurns)
        {
            var sw = new Stopwatch();
            sw.Start();
            var crabCup = new CrabCup(initialState, 1000000);
            var percent = numberOfTurns / 100;
            for (int i = 0; i < numberOfTurns; i++)
            {
                if (i % percent == 0)
                {
                    Console.WriteLine($"{i / percent}%      ({sw.Elapsed})");
                }
                crabCup.Move();
            }
            sw.Stop();
            Console.WriteLine($"Ellapsed time: {sw.Elapsed}");

            return crabCup.GetAdvancedSignature();
        }
    }
}
