﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode2020.Day23
{
    class CrabCup
    {
        private readonly CupCircle cups;
        public CrabCup(string initialState)
        {
            cups = new CupCircle(initialState.Select(c => c - '0').ToArray());
            //cups.Print();
        }

        public CrabCup(string initialState, int maxSize)
        {
            var numbers = new List<int>(initialState.Select(c => c - '0'));
            for(int i = initialState.Length + 1; i < maxSize + 1; i++)
            {
                numbers.Add(i);
            }

            cups = new CupCircle(numbers.ToArray());
        }

        public void Move()
        {
            var currentCup = cups.CurrentCup;
            var cupsToMove = new[]
            {
                cups.Get(cups.CurrentPosition + 1),
                cups.Get(cups.CurrentPosition + 2),
                cups.Get(cups.CurrentPosition + 3),
            };

            var targetCup = cups.CalculateTargetCup(currentCup, cupsToMove);
            //System.Console.WriteLine($"Target cup: {targetCup}");

            int targetIndex = cups.ShiftToTarget(targetCup);

            for (int i = 0; i < 3; i++)
            {
                cups.Set(targetIndex + i, cupsToMove[i]);
            }

            cups.CurrentPosition++;

            //System.Console.WriteLine("End of move:");
            //cups.Print();
        }

        public string GetSignature()
        {
            var index = 0;
            while(index < cups.Count)
            {
                if (cups.Get(index) == 1)
                {

                    break;
                }
                index++;
            }

            var builder = new StringBuilder();
            for(int i = 0; i < cups.Count - 1; i++)
            {
                builder.Append(cups.Get(index + i + 1));
            }

            return builder.ToString();
        }

        public long GetAdvancedSignature()
        {
            var index = 0;
            while (index < cups.Count)
            {
                if (cups.Get(index) == 1)
                {

                    break;
                }
                index++;
            }

            return (long)cups.Get(index + 1) * cups.Get(index + 2);
        }
    }
}
