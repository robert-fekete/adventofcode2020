﻿using System;
using System.Linq;

namespace AdventOfCode2020.Day23
{
    class CupCircle
    {
        private readonly int min;
        private readonly int max;
        private readonly int[] cups;
        private int currentPosition = 0;

        public CupCircle(int[] initialState)
        {
            cups = initialState;
            min = cups.Min();
            max = cups.Max();
        }

        public int Count => cups.Length;
        public int CurrentCup => Get(CurrentPosition);
        public int CurrentPosition
        {
            get
            {
                return currentPosition;
            }
            set
            {
                currentPosition = value % cups.Length;
            }
        }

        public void Set(int index, int cup)
        {
            var i = index % cups.Length;
            cups[i] = cup;
        }

        internal int CalculateTargetCup(int currentCup, int[] cupsToMove)
        {
            var targetCup = currentCup;

            do
            {
                targetCup--;
                if (targetCup < min)
                {
                    targetCup = max;
                }
            }
            while (cupsToMove.Contains(targetCup));

            return targetCup;
        }

        public int ShiftToTarget(int targetCup)
        {
            var shiftFrom = currentPosition + 3;
            var shiftTo = currentPosition;

            //System.Console.WriteLine("Shifting...");
            int lastMoved;
            do
            {
                shiftFrom++;
                shiftTo++;
                
                if (shiftFrom >= cups.Length)
                {
                    shiftFrom -= cups.Length;
                }
                if (shiftTo >= cups.Length)
                {
                    shiftTo -= cups.Length;
                }

                lastMoved = cups[shiftFrom];
                cups[shiftTo] = lastMoved;
                //cups.Print();
            }
            while (lastMoved != targetCup);

            return shiftTo + 1;
        }

        public int Get(int index)
        {
            var i = index % cups.Length;
            return cups[i];
        }

        public void Print()
        {
            Print(cups.Length);
        }

        public void Print(int num)
        {
            for (int i = 0; i < num; i++)
            {
                if (i == CurrentPosition)
                {
                    Console.Write($"({cups[i]}) ");
                }
                else
                {
                    Console.Write($"{cups[i]} ");
                }
            }
            Console.WriteLine();
        }
    }
}
