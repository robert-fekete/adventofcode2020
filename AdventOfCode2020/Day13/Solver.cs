﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace AdventOfCode2020.Day13
{
    class Solver : ISolver
    {
        public int DayNumber => 13;

        public string FirstExpected => "4782";

        public string SecondExpected => "1118684865113056";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "939",
                    "7,13,x,x,59,x,31,19"
                }, "295")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "939",
                    "7,13,x,x,59,x,31,19"
                }, "1068781")
                .AddTestCase(new[]
                {
                    "939",
                    "17,x,13,19"
                }, "3417")
                .AddTestCase(new[]
                {
                    "939",
                    "67,7,59,61"
                }, "754018")
                .AddTestCase(new[]
                {
                    "939",
                    "67,x,7,59,61"
                }, "779210")
                .AddTestCase(new[]
                {
                    "939",
                    "67,7,x,59,61"
                }, "1261476")
                .AddTestCase(new[]
                {
                    "939",
                    "1789,37,47,1889"
                }, "1202161486")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var timestamp = long.Parse(input.First());
            
            var busses = ParseBusses(input);
            var id = busses.OrderBy(p => p.Id - (timestamp % p.Id)).First().Id;

            var minutesToWait = id - (timestamp % id);
            var result = id * minutesToWait;

            return result.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var busses = ParseBusses(input);

            var timestamp = 0L;
            var increment = 1L;
            foreach ((var id, var delay) in busses)
            {
                while ((timestamp + delay) % id != 0)
                {
                    timestamp += increment;
                }
                increment *= id;
            }

            return timestamp.ToString();
        }

        private List<(long Id, long Delay)> ParseBusses(IEnumerable<string> input)
        {
            var tokens = input.Skip(1).First().Split(",");

            var busses = new List<(long, long)>();
            for (int i = 0; i < tokens.Length; i++)
            {
                if (tokens[i] != "x")
                {
                    var busId = long.Parse(tokens[i]);
                    var delay = (long)i;
                    busses.Add((busId, delay));
                }
            }

            return busses;
        }
    }
}
