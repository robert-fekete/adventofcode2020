﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;

namespace AdventOfCode2020.Day24
{
    class Solver : ISolver
    {
        public int DayNumber => 24;

        public string FirstExpected => "332";

        public string SecondExpected => "3900";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestBuilder(b =>
                    b.AddTestCase("nwwswee", (0m, 0m))
                    .AddTestCase("esew", (0.5m, 1m))
                    .AddTest(TraceDirections)
                )
                .AddTestBuilder(b => 
                    b.AddTestCase(new[]
                    {
                        "sesenwnenenewseeswwswswwnenewsewsw",
                        "neeenesenwnwwswnenewnwwsewnenwseswesw",
                        "seswneswswsenwwnwse",
                        "nwnwneseeswswnenewneswwnewseswneseene",
                        "swweswneswnenwsewnwneneseenw",
                        "eesenwseswswnenwswnwnwsewwnwsene",
                        "sewnenenenesenwsewnenwwwse",
                        "wenwwweseeeweswwwnwwe",
                        "wsweesenenewnwwnwsenewsenwwsesesenwne",
                        "neeswseenwwswnwswswnw",
                        "nenwswwsewswnenenewsenwsenwnesesenew",
                        "enewnwewneswsewnwswenweswnenwsenwsw",
                        "sweneswneswneneenwnewenewwneswswnese",
                        "swwesenesewenwneswnwwneseswwne",
                        "enesenwswwswneneswsenwnewswseenwsese",
                        "wnwnesenesenenwwnenwsewesewsesesew",
                        "nenewswnwewswnenesenwnesewesw",
                        "eneswnwswnwsenenwnwnwwseeswneewsenese",
                        "neswnwewnwnwseenwseesewsenwsweewe",
                        "wseweeenwnesenwwwswnew",
                    }, "10")
                    .AddTest(ExecuteFirst)
                )
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "sesenwnenenewseeswwswswwnenewsewsw",
                    "neeenesenwnwwswnenewnwwsewnenwseswesw",
                    "seswneswswsenwwnwse",
                    "nwnwneseeswswnenewneswwnewseswneseene",
                    "swweswneswnenwsewnwneneseenw",
                    "eesenwseswswnenwswnwnwsewwnwsene",
                    "sewnenenenesenwsewnenwwwse",
                    "wenwwweseeeweswwwnwwe",
                    "wsweesenenewnwwnwsenewsenwwsesesenwne",
                    "neeswseenwwswnwswswnw",
                    "nenwswwsewswnenenewsenwsenwnesesenew",
                    "enewnwewneswsewnwswenweswnenwsenwsw",
                    "sweneswneswneneenwnewenewwneswswnese",
                    "swwesenesewenwneswnwwneseswwne",
                    "enesenwswwswneneswsenwnewswseenwsese",
                    "wnwnesenesenenwwnenwsewesewsesesew",
                    "nenewswnwewswnenesenwnesewesw",
                    "eneswnwswnwsenenwnwnwwseeswneewsenese",
                    "neswnwewnwnwseenwseesewsenwsweewe",
                    "wseweeenwnesenwwwswnew",
                }, "2208")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var blacks = GetBlackTiles(input);

            return blacks.Count.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var offsets = new[] { (-1m, 0m), (-0.5m, -1m), (0.5m, -1m), (1m, 0), (0.5m, 1m), (-0.5m, 1m) };
            var blacks = GetBlackTiles(input);

            for(int i = 0; i < 100; i++)
            {
                var candidates = GetFlipCandidates(blacks, offsets);

                var newBlacks = new HashSet<(decimal, decimal)>();
                foreach((var x, var y) in candidates)
                {
                    var count = CountNeighbours(blacks, (x, y), offsets);
                    if (blacks.Contains((x, y)) && 0 < count && count <= 2)
                    {
                        newBlacks.Add((x, y));
                    }
                    else if (!blacks.Contains((x, y)) && count == 2)
                    {
                        newBlacks.Add((x, y));
                    }
                }

                blacks = newBlacks;
            }

            return blacks.Count.ToString();
        }

        private int CountNeighbours(HashSet<(decimal, decimal)> blacks, (decimal X, decimal Y) position, (decimal, decimal)[] offsets)
        {
            var count = 0;
            foreach ((var dx, var dy) in offsets)
            {
                // TODO save these (x, y)-s as the candidates for the new round
                var x = position.X + dx;
                var y = position.Y + dy;
                if (blacks.Contains((x, y)))
                {
                    count++;
                }
            }

            return count;
        }

        private IEnumerable<(decimal, decimal)> GetFlipCandidates(HashSet<(decimal, decimal)> blacks, (decimal, decimal)[] offsets)
        {
            var candidates = new HashSet<(decimal, decimal)>();
            foreach((var x, var y) in blacks)
            {
                foreach((var dx, var dy) in offsets)
                {
                    candidates.Add((x + dx, y + dy));
                }
            }

            return candidates;
        }

        private HashSet<(decimal, decimal)> GetBlackTiles(IEnumerable<string> input)
        {
            var blacks = new HashSet<(decimal, decimal)>();
            foreach (var line in input)
            {
                var position = TraceDirections(line);
                if (blacks.Contains(position))
                {
                    blacks.Remove(position);
                }
                else
                {
                    blacks.Add(position);
                }
            }

            return blacks;
        }

        private (decimal, decimal) TraceDirections(string directions)
        {
            (decimal X, decimal Y) position = (0, 0);
            for (int i = 0; i < directions.Length; i++)
            {
                if (directions[i] == 'w')
                {
                    position.X -= 1;
                }
                else if (directions[i] == 'e')
                {
                    position.X += 1;
                }
                else
                {
                    if (directions[i] == 's')
                    {
                        position.Y += 1;
                    }
                    else if (directions[i] == 'n')
                    {
                        position.Y -= 1;
                    }
                    else
                    {
                        throw new InvalidOperationException($"Invalid directions: {directions[i]}");
                    }

                    if (directions[i + 1] == 'w')
                    {
                        position.X -= 0.5m;
                    }
                    else if (directions[i + 1] == 'e')
                    {
                        position.X += 0.5m;
                    }
                    else
                    {
                        throw new InvalidOperationException($"Invalid directions (sub): {directions[i + 1]}");
                    }
                    i++;
                }
            }

            return position;
        }
    }
}
