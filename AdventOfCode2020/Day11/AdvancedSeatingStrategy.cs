﻿namespace AdventOfCode2020.Day11
{
    class AdvancedSeatingStrategy : ISeatingStrategy
    {
        public int MaxNeighbours => 5;

        public bool IsOccupied(Cell[][] layout, int x, int y, int dx, int dy)
        {
            var tempX = x + dx;
            var tempY = y + dy;
            while (IsCellType(layout, tempX, tempY, Cell.Floor))
            {
                tempY += dy;
                tempX += dx;
            }

            return IsCellType(layout, tempX, tempY, Cell.OccupiedChair);
        }

        private bool IsCellType(Cell[][] layout, int tempX, int tempY, Cell cellType)
        {
            return IsInRange(tempX, layout[0].Length) &&
                IsInRange(tempY, layout.Length) &&
                layout[tempY][tempX] == cellType;
        }

        private bool IsInRange(int value, int maxValue)
        {
            return value >= 0 && value < maxValue;
        }
    }
}
