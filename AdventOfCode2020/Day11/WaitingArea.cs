﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day11
{
    class WaitingArea
    {
        private readonly (int dx, int dy)[] offsets = new[] { (-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1) };
        private Cell[][] layout;
        private readonly ISeatingStrategy seatingStrategy;

        public WaitingArea(Cell[][] layout, ISeatingStrategy seatingStrategy)
        {
            this.layout = layout;
            this.seatingStrategy = seatingStrategy;
        }

        public int OccupiedChairs => layout.Select(row => row.Count(c => c == Cell.OccupiedChair)).Sum();

        public bool SimulateRound()
        {
            var isChanged = false;
            var newLayout = new List<List<Cell>>();
            for(int y = 0; y < layout.Length; y++)
            {
                var row = new List<Cell>();
                for (int x = 0; x < layout[0].Length; x++)
                {
                    var newSeat = GetNewSeat(x, y);
                    row.Add(newSeat);

                    if (layout[y][x] != newSeat)
                    {
                        isChanged = true;
                    }
                }

                newLayout.Add(row);
            }

            layout = newLayout.Select(r => r.ToArray()).ToArray();

            return isChanged;
        }

        private Cell GetNewSeat(int x, int y)
        {
            Cell currentCell = layout[y][x];
            if (currentCell == Cell.Floor)
            {
                return Cell.Floor;
            }
            else if (currentCell == Cell.EmptyChair)
            {
                var occupiedSeats = CountOccupiedSeats(x, y);
                return occupiedSeats == 0 ? Cell.OccupiedChair : Cell.EmptyChair;
            }
            else if (currentCell == Cell.OccupiedChair)
            {
                var occupiedSeats = CountOccupiedSeats(x, y);
                return occupiedSeats >= seatingStrategy.MaxNeighbours ? Cell.EmptyChair : Cell.OccupiedChair;
            }

            throw new InvalidOperationException($"Invalid cell type {currentCell}");
        }

        private int CountOccupiedSeats(int x, int y)
        {
            var occupiedSeats = 0;
            foreach ((var dx, var dy) in offsets)
            {
                if (seatingStrategy.IsOccupied(layout, x, y, dx, dy))
                {
                    occupiedSeats++;
                }
            }

            return occupiedSeats;
        }
    }

    enum Cell
    {
        Floor,
        EmptyChair,
        OccupiedChair
    }
}
