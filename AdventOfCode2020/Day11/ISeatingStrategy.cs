﻿namespace AdventOfCode2020.Day11
{
    interface ISeatingStrategy
    {
        int MaxNeighbours { get; }

        bool IsOccupied(Cell[][] layout, int x, int y, int dx, int dy);
    }
}
