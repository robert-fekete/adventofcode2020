﻿namespace AdventOfCode2020.Day11
{
    class BasicSeatingStrategy : ISeatingStrategy
    {
        public int MaxNeighbours => 4;

        public bool IsOccupied(Cell[][] layout, int x, int y, int dx, int dy)
        {
            var newX = x + dx;
            var newY = y + dy;

            return newX >= 0 && newX < layout[0].Length &&
                newY >= 0 && newY < layout.Length &&
                layout[newY][newX] == Cell.OccupiedChair;
        }
    }
}
