﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day11
{
    class Solver : ISolver
    {
        public int DayNumber => 11;

        public string FirstExpected => "2324";

        public string SecondExpected => "2068";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "L.LL.LL.LL",
                    "LLLLLLL.LL",
                    "L.L.L..L..",
                    "LLLL.LL.LL",
                    "L.LL.LL.LL",
                    "L.LLLLL.LL",
                    "..L.L.....",
                    "LLLLLLLLLL",
                    "L.LLLLLL.L",
                    "L.LLLLL.LL",
                }, "37")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "L.LL.LL.LL",
                    "LLLLLLL.LL",
                    "L.L.L..L..",
                    "LLLL.LL.LL",
                    "L.LL.LL.LL",
                    "L.LLLLL.LL",
                    "..L.L.....",
                    "LLLLLLLLLL",
                    "L.LLLLLL.L",
                    "L.LLLLL.LL",
                }, "26")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var layout = ParseLayout(input);
            var waitingArea = new WaitingArea(layout, new BasicSeatingStrategy());

            while (waitingArea.SimulateRound()) { }

            var numberOfChairs = waitingArea.OccupiedChairs;
            return numberOfChairs.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var layout = ParseLayout(input);
            var waitingArea = new WaitingArea(layout, new AdvancedSeatingStrategy());

            while (waitingArea.SimulateRound()) { }

            var numberOfChairs = waitingArea.OccupiedChairs;
            return numberOfChairs.ToString();
        }

        private static Cell[][] ParseLayout(IEnumerable<string> input)
        {
            var layout = new List<List<Cell>>();
            foreach (var line in input)
            {
                var row = new List<Cell>();
                foreach (var chr in line)
                {
                    if (chr == '.')
                    {
                        row.Add(Cell.Floor);
                    }
                    else if (chr == 'L')
                    {
                        row.Add(Cell.EmptyChair);
                    }
                    else
                    {
                        throw new InvalidOperationException("Invalid cell");
                    }
                }
                layout.Add(row);
            }

            return layout.Select(r => r.ToArray()).ToArray();
        }
    }
}
