﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day8
{
    class Solver : ISolver
    {
        public int DayNumber => 8;

        public string FirstExpected => "1814";

        public string SecondExpected => "1056";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "nop +0",
                    "acc +1",
                    "jmp +4",
                    "acc +3",
                    "jmp -3",
                    "acc -99",
                    "acc +1",
                    "jmp -4",
                    "acc +6",
                }
                , "5")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "nop +0",
                    "acc +1",
                    "jmp +4",
                    "acc +3",
                    "jmp -3",
                    "acc -99",
                    "acc +1",
                    "jmp -4",
                    "acc +6",
                }
                , "8")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var code = ParseInstructions(input);
            var processor = new Processor(code);
            var instructionPointers = new HashSet<int>();

            var accumulator = processor.Execute(ip =>
            {
                if (instructionPointers.Contains(ip))
                {
                    return false;
                }
                else
                {
                    instructionPointers.Add(ip);
                    return true;
                }
            });

            return accumulator.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var code = ParseInstructions(input);
            var graph = ParseGraph(code);
            var traversal = new GraphTraversal();
            var id = traversal.GetCorruptedId(graph, code.Count, 0);

            var newCode = input.ToArray();            
            if (newCode[id].StartsWith("jmp"))
            {
                newCode[id] = newCode[id].Replace("jmp", "nop");
            }
            else if(newCode[id].StartsWith("nop"))
            {
                newCode[id] = newCode[id].Replace("nop", "jmp");
            }

            return ExecuteFirst(newCode);
        }

        private IList<(string, int)> ParseInstructions(IEnumerable<string> input)
        {
            var code = new List<(string, int)>();
            foreach(var line in input)
            {
                var parts = line.Split(" ");
                var command = parts[0];
                var argument = int.Parse(parts[1]);

                code.Add((command, argument));
            }

            return code;
        }

        private Dictionary<int, IReadOnlyCollection<(int, bool)>> ParseGraph(IList<(string, int)> code)
        {
            var graph = new Dictionary<int, List<(int, bool)>>();
            for (var id = 0; id < code.Count; id++)
            {
                (var command, var argument) = code[id];

                if (command == "acc")
                {
                    AddEdge(graph, id + 1, (id, true));
                }
                else if (command == "jmp")
                {
                    var targetId = Math.Min(code.Count, id + argument);
                    AddEdge(graph, targetId, (id, true));
                    AddEdge(graph, id + 1, (id, false)); // If this were a nop
                }
                else if (command == "nop")
                {
                    var targetId = Math.Min(code.Count, id + argument);
                    AddEdge(graph, targetId, (id, false)); // If this were a jmp
                    AddEdge(graph, id + 1, (id, true));
                }
            }

            return graph.ToDictionary<KeyValuePair<int, List<(int, bool)>>, int, IReadOnlyCollection<(int, bool)>>(kvp => kvp.Key, kvp => kvp.Value);
        }

        private void AddEdge(Dictionary<int, List<(int, bool)>> graph, int from, (int, bool) to)
        {
            if (!graph.ContainsKey(from))
            {
                graph[from] = new List<(int, bool)>();
            }

            graph[from].Add(to);
        }
    }
}
