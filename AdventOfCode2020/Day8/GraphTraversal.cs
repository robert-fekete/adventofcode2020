﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day8
{
    class GraphTraversal
    {
        public int GetCorruptedId(Dictionary<int, IReadOnlyCollection<(int, bool)>> graph, int startingNode, int targetNode)
        {
            var backlog = new Queue<(int, int?)>();
            backlog.Enqueue((startingNode, null));

            while (backlog.Any())
            {
                (var currentNode, var corruptedId) = backlog.Dequeue();

                if (currentNode == targetNode)
                {
                    if (corruptedId.HasValue)
                    {
                        return corruptedId.Value;
                    }
                    else
                    {
                        throw new InvalidOperationException("There wasn't any infinite loop in the code");
                    }
                }

                foreach ((var next, var isRealEdge) in graph[currentNode])
                {
                    if (!isRealEdge)
                    {
                        if (corruptedId is null)
                        {
                            backlog.Enqueue((next, next));
                        }
                    }
                    else
                    {
                        backlog.Enqueue((next, corruptedId));
                    }
                }
            }

            throw new InvalidOperationException("Couldn't find corrupted instruction");
        }
    }
}
