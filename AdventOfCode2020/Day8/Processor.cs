﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2020.Day8
{
    class Processor
    {
        private readonly IList<(string, int)> code;
        private int instructionPointer = 0;
        private int accumulator = 0;

        public Processor(IList<(string, int)> code)
        {
            this.code = code;
        }

        public int Execute(Func<int, bool> callback)
        {
            while (instructionPointer < code.Count && callback(instructionPointer))
            {
                ExecuteInstruction();
                instructionPointer++;
            }

            return accumulator;
        }

        private void ExecuteInstruction()
        {
            (var command, var argument) = code[instructionPointer];
            
            if (command == "acc")
            {
                accumulator += argument;
            }
            else if(command == "jmp")
            {
                instructionPointer += argument;
                instructionPointer--; // Correction for the auto increment in the execution loop
            }
        }
    }
}
