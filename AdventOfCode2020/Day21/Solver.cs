﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day21
{
    class Solver : ISolver
    {
        public int DayNumber => 21;

        public string FirstExpected => "2423";

        public string SecondExpected => "jzzjz,bxkrd,pllzxb,gjddl,xfqnss,dzkb,vspv,dxvsp";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)",
                    "trh fvjkl sbzzf mxmxvkd (contains dairy)",
                    "sqjhc fvjkl (contains soy)",
                    "sqjhc mxmxvkd sbzzf (contains fish)",
                }, "5")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)",
                    "trh fvjkl sbzzf mxmxvkd (contains dairy)",
                    "sqjhc fvjkl (contains soy)",
                    "sqjhc mxmxvkd sbzzf (contains fish)",
                }, "mxmxvkd,sqjhc,fvjkl")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var foods = ParseFoods(input);
            var identifiedAllergen = IdentifyAllergens(foods);
            
            var count = 0;
            foreach ((var ingredients, var _) in foods)
            {
                count += ingredients.Count(i => !identifiedAllergen.Values.Contains(i));
            }

            return count.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var foods = ParseFoods(input);
            var identifiedAllergen = IdentifyAllergens(foods);

            var canonicalList = string.Join(",", identifiedAllergen.OrderBy(kvp => kvp.Key).Select(kvp => kvp.Value));

            return canonicalList;
        }

        private static Dictionary<string, string> IdentifyAllergens(IReadOnlyCollection<(IReadOnlyCollection<string> Ingredients, IReadOnlyCollection<string> Allergens)> foods)
        {
            var pairing = new Dictionary<string, List<HashSet<string>>>();
            foreach ((var ingredients, var allergens) in foods)
            {
                foreach (var allergen in allergens)
                {
                    if (!pairing.ContainsKey(allergen))
                    {
                        pairing[allergen] = new List<HashSet<string>>();
                    }
                    pairing[allergen].Add(new HashSet<string>(ingredients));
                }
            }

            var identifiedAllergen = new Dictionary<string, string>();
            var changed = true;
            while (changed)
            {
                changed = false;
                foreach (var kvp in pairing)
                {
                    var allergen = kvp.Key;
                    if (identifiedAllergen.ContainsKey(allergen))
                    {
                        continue;
                    }

                    var foodIngredients = kvp.Value;
                    var intersect = foodIngredients.First();
                    intersect.ExceptWith(identifiedAllergen.Values);
                    foreach (var food in foodIngredients.Skip(1))
                    {
                        food.ExceptWith(identifiedAllergen.Values);
                        intersect.IntersectWith(food);
                    }

                    if (intersect.Count == 1)
                    {
                        identifiedAllergen[allergen] = intersect.First();
                        changed = true;
                    }
                }
            }

            return identifiedAllergen;
        }

        private IReadOnlyCollection<(IReadOnlyCollection<string> Ingredients, IReadOnlyCollection<string> Allergens)> ParseFoods(IEnumerable<string> input)
        {
            var foods = new List<(IReadOnlyCollection<string>, IReadOnlyCollection<string>)>();
            foreach(var line in input)
            {
                var parts = line.Split("(contains ");
                var ingredients = parts[0].Trim().Split(' ');
                var allergens = parts[1].Trim(')').Split(", ");

                foods.Add((ingredients, allergens));
            }

            return foods;
        }
    }
}
