﻿namespace AdventOfCode2020.Day17
{
    internal class HashAbleArray
    {
        private int[] coordinates;

        public HashAbleArray(int[] coordinates)
        {
            this.coordinates = coordinates;
        }

        public int this[int i]
        {
            get
            {
                return coordinates[i];
            }
        }

        public override bool Equals(object? obj)
        {
            if (!(obj is HashAbleArray other) || other == null)
            {
                return false;
            }

            for(int i = 0; i < coordinates.Length; i++)
            {
                if (coordinates[i] != other.coordinates[i])
                {
                    return false;
                }
            }
            return true;
        }

        public override int GetHashCode()
        {
            var hash = 0;
            foreach(var coordinate in coordinates)
            {
                hash *= 100;
                hash += coordinate;
            }

            return hash;
        }
    }
}