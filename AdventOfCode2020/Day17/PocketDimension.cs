﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day17
{
    class PocketDimension
    {
        private readonly Stack<int> coordinateStack = new Stack<int>();

        private readonly int dimensions;
        private readonly int[][] offsets;

        private Dictionary<HashAbleArray, bool> readPlanes = new Dictionary<HashAbleArray, bool>();
        private Dictionary<HashAbleArray, bool> writePlanes = new Dictionary<HashAbleArray, bool>();

        public PocketDimension(int dimensions, IEnumerable<(int, int)> initialState)
        {
            this.dimensions = dimensions;
            offsets = GenerateOffsets();
            InitializePlanes(initialState);
        }

        public int GetActiveCubes()
        {
            return readPlanes.Values.Count(c => c);
        }

        public void SimulateCycle()
        {
            writePlanes = new Dictionary<HashAbleArray, bool>();
            SimulateCycleRec(0);
            readPlanes = writePlanes;
        }

        private void SimulateCycleRec(int currentDimension)
        {
            if (currentDimension < dimensions)
            {
                var min = readPlanes.Keys.Min(c => c[currentDimension]);
                var max = readPlanes.Keys.Max(c => c[currentDimension]);
                for(int i = min - 1; i <= max + 1; i++)
                {
                    coordinateStack.Push(i);
                    SimulateCycleRec(currentDimension + 1);
                    coordinateStack.Pop();
                }
            }
            else
            {
                var coordinates = coordinateStack.ToArray();
                var count = offsets.Count(p => IsActive(coordinates.Zip(p).Select(p => p.First + p.Second).ToArray()));
                if (IsActive(coordinates))
                {
                    if (count == 2 || count == 3)
                    {
                        Activate(coordinates);
                    }
                    else
                    {
                        Deactivate(coordinates);
                    }
                }
                else
                {
                    if (count == 3)
                    {
                        Activate(coordinates);
                    }
                    else
                    {
                        Deactivate(coordinates);
                    }
                }
            }
        }

        private void Deactivate(int[] coordinates)
        {
            writePlanes[new HashAbleArray(coordinates)] = false;
        }

        private void Activate(int[] coordinates)
        {
            writePlanes[new HashAbleArray(coordinates)] = true;
        }

        private bool IsActive(int[] coordinates)
        {
            var key = new HashAbleArray(coordinates);
            return readPlanes.ContainsKey(key) && readPlanes[key];
        }

        private int[][] GenerateOffsets()
        {
            var offsets = new List<int[]>()
            {
                new[] { 1 },
                new[] { 0 },
                new[] { -1 }
            };
            var deltas = new[] { -1, 0, 1 };
            for (int i = 1; i < dimensions; i++)
            {
                var newOffsets = new List<int[]>();
                foreach (var offset in offsets)
                {
                    foreach (var delta in deltas)
                    {
                        newOffsets.Add(offset.Append(delta).ToArray());
                    }
                }
                offsets = newOffsets;
            }

            return offsets.Where(o => !o.All(delta => delta == 0)).ToArray();
        }

        private void InitializePlanes(IEnumerable<(int, int)> initialState)
        {
            foreach ((var x, var y) in initialState)
            {
                var coordinates = new List<int>() { x, y };
                for (int i = 2; i < dimensions; i++)
                {
                    coordinates.Add(0);
                }
                readPlanes[new HashAbleArray(coordinates.ToArray())] = true;
            }
        }
    }
}
