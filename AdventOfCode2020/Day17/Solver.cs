﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day17
{
    class Solver : ISolver
    {
        public int DayNumber => 17;

        public string FirstExpected => "426";

        public string SecondExpected => "1892";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    ".#.",
                    "..#",
                    "###",
                }, "112")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    ".#.",
                    "..#",
                    "###",
                }, "848")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var offsets = GenerateOffest();
            var planes = new Dictionary<(int, int, int), bool>();

            var lines = input.ToArray();
            for(int y = 0; y < lines.Length; y++)
            {
                for (int x = 0; x < lines[y].Length; x++)
                {
                    if (lines[y][x] == '#')
                    {
                        planes[(x, y, 0)] = true;
                    }
                }
            }

            for (int i = 0; i < 6; i++)
            {
                var minX = planes.Keys.Min(v => v.Item1);
                var maxX = planes.Keys.Max(v => v.Item1);
                var minY = planes.Keys.Min(v => v.Item2);
                var maxY = planes.Keys.Max(v => v.Item2);
                var minZ = planes.Keys.Min(v => v.Item3);
                var maxZ = planes.Keys.Max(v => v.Item3);

                var newPlanes = new Dictionary<(int, int, int), bool>();
                for (int x = minX - 1; x <= maxX + 1; x++)
                {
                    for (int y = minY - 1; y <= maxY + 1; y++)
                    {
                        for (int z = minZ - 1; z <= maxZ + 1; z++)
                        {
                            var count = offsets.Count(p => IsValidCube(planes, (x + p.Item1, y + p.Item2, z + p.Item3)));
                            if (IsValidCube(planes, (x, y, z)))
                            {
                                if (count == 2 || count == 3)
                                {
                                    newPlanes[(x, y, z)] = true;
                                }
                                else
                                {
                                    newPlanes[(x, y, z)] = false;
                                }
                            }
                            else
                            {
                                if (count == 3)
                                {
                                    newPlanes[(x, y, z)] = true;
                                }
                                else
                                {
                                    newPlanes[(x, y, z)] = false;
                                }
                            }
                        }
                    }
                }
                planes = newPlanes;
            }

            return planes.Values.Count(v => v).ToString();
        }

        private static bool IsValidCube(Dictionary<(int, int, int), bool> planes, (int, int, int) coordinates)
        {
            return planes.ContainsKey(coordinates) && planes[coordinates];
        }

        private IReadOnlyCollection<(int, int, int)> GenerateOffest()
        {
            var offsets = new List<(int, int, int)>();
            foreach(var dx in new[] { 1, 0, -1 })
            {
                foreach (var dy in new[] { 1, 0, -1 })
                {
                    foreach (var dz in new[] { 1, 0, -1 })
                    {
                        offsets.Add((dx, dy, dz));
                    }
                }
            }

            offsets.Remove((0, 0, 0));
            return offsets;
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var initialState = ParseInitialState(input);
            var pocketDimension = new PocketDimension(4, initialState);

            for(int i = 0; i < 6; i++)
            {
                pocketDimension.SimulateCycle();
            }

            var activeCubes = pocketDimension.GetActiveCubes();
            return activeCubes.ToString();
        }

        private static IReadOnlyCollection<(int, int)> ParseInitialState(IEnumerable<string> input)
        {
            var initialState = new List<(int, int)>();
            var lines = input.ToArray();
            for (int y = 0; y < lines.Length; y++)
            {
                for (int x = 0; x < lines[y].Length; x++)
                {
                    if (lines[y][x] == '#')
                    {
                        initialState.Add((x, y));
                    }
                }
            }

            return initialState;
        }
    }
}
