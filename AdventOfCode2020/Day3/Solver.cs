﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day3
{
    class Solver : ISolver
    {
        public int DayNumber => 3;

        public string FirstExpected => "193";

        public string SecondExpected => "1355323200";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "..##.......",
                    "#...#...#..",
                    ".#....#..#.",
                    "..#.#...#.#",
                    ".#...##..#.",
                    "..#.##.....",
                    ".#.#.#....#",
                    ".#........#",
                    "#.##...#...",
                    "#...##....#",
                    ".#..#...#.#",
                }
                , (3, 1), 7)
                .AddTest(CountTrees)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "..##.......",
                    "#...#...#..",
                    ".#....#..#.",
                    "..#.#...#.#",
                    ".#...##..#.",
                    "..#.##.....",
                    ".#.#.#....#",
                    ".#........#",
                    "#.##...#...",
                    "#...##....#",
                    ".#..#...#.#",
                }, "336")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return CountTrees(input, (3, 1)).ToString();
        }

        private int CountTrees(IEnumerable<string> map, (int X, int Y) slope)
        {
            var forest = new Forest(map);
            var x = 0;
            var y = 0;
            var counter = 0;
            while (!forest.IsEnded(y))
            {
                if (forest.IsTree(x, y))
                {
                    counter++;
                }
                x += slope.X;
                y += slope.Y;
            }

            return counter;
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var slopes = new[]
            {
                (1, 1),
                (3, 1),
                (5, 1),
                (7, 1),
                (1, 2)
            };
            var total = 1L;
            foreach(var slope in slopes)
            {
                var result = CountTrees(input, slope);
                total *= result;
            }

            return total.ToString();
        }
    }
}
