﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day3
{
    internal class Forest
    {
        private string[] map;
        private readonly int width;
        private readonly int height;

        public Forest(IEnumerable<string> map)
        {
            this.map = map.ToArray();
            width = this.map[0].Length;
            height = this.map.Length;
        }

        internal bool IsEnded(int y) => y >= height;

        internal bool IsTree(int x, int y) => map[y][x % width] == '#';
    }
}