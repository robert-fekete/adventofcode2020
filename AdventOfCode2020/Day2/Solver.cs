﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day2
{
    class Solver : ISolver
    {
        public int DayNumber => 2;

        public string FirstExpected => "542";

        public string SecondExpected => "360";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase("1-3 a: abcde", true)
                .AddTestCase("1-3 b: cdefg", false)
                .AddTestCase("2-9 c: ccccccccc", true)
                .AddTest(IsSledRentalPasswordValid)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase("1-3 a: abcde", true)
                .AddTestCase("1-3 b: cdefg", false)
                .AddTestCase("2-9 c: ccccccccc", false)
                .AddTest(IsTobogganRentalPasswordValid)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return input.Count(IsSledRentalPasswordValid).ToString();
        }

        private bool IsSledRentalPasswordValid(string line)
        {
            var parts = line.Split(':');
            (var policy, var password) = (parts[0], parts[1]);

            var policyParts = policy.Split(' ');
            var letter = policyParts[1][0];
            var rangeParts = policyParts[0].Split('-');
            var min = int.Parse(rangeParts[0]);
            var max = int.Parse(rangeParts[1]);

            var count = password.Count(l => l == letter);
            return min <= count && count <= max;
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return input.Count(IsTobogganRentalPasswordValid).ToString();
        }

        private bool IsTobogganRentalPasswordValid(string line)
        {
            var parts = line.Split(':');
            (var policy, var password) = (parts[0], parts[1]);

            var policyParts = policy.Split(' ');
            var letter = policyParts[1][0];
            var rangeParts = policyParts[0].Split('-');
            var a = int.Parse(rangeParts[0]);
            var b = int.Parse(rangeParts[1]);

            var hasA = password[a] == letter;
            var hasB = password[b] == letter;

            return (hasA && !hasB) || (!hasA && hasB);
        }
    }
}
