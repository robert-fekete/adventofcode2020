﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day20
{
    class PatternFinder
    {
        private readonly PixelMatrix image;

        public PatternFinder(PixelMatrix image)
        {
            this.image = image;
        }

        public int CountPattern(string[] pattern)
        {
            var patternOffsets = CalculatePatternOffsets(pattern, (0, 1));

            var count = 0;
            for(int y = 0; y < image.Height; y++)
            {
                for(int x = 0; x < image.Width; x++)
                {
                    if (IsBlack(image.GetPixel((x, y))))
                    {
                        if (patternOffsets.All(o => IsValidCoordinate((x + o.X, y + o.Y)) && IsBlack(image.GetPixel((x + o.X, y + o.Y)))))
                        {
                            count++;
                        }
                    }
                }
            }

            return count;
        }

        private bool IsValidCoordinate((int X, int Y) coordinate)
        {
            return (0 <= coordinate.X && coordinate.X < image.Width) && (0 <= coordinate.Y && coordinate.Y < image.Height);
        }

        private IReadOnlyCollection<(int X, int Y)> CalculatePatternOffsets(string[] pattern, (int X, int Y) origo)
        {
            var offsets = new List<(int, int)>();
            for(int y  = 0; y < pattern.Length; y++)
            {
                for (int x = 0; x < pattern[0].Length; x++)
                {
                    if (IsBlack(pattern[y][x]))
                    {
                        var offsetX = x - origo.X;
                        var offsetY = y - origo.Y;
                        offsets.Add((offsetX, offsetY));
                    }
                }
            }

            return offsets;
        }

        private bool IsBlack(char pixel)
        {
            return pixel == '#';
        }
    }
}
