﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2020.Day20
{
    class Solver : ISolver
    {
        public int DayNumber => 20;

        public string FirstExpected => "79412832860579";

        public string SecondExpected => "2155";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(File.ReadAllLines(".\\Day20\\test_input.txt"), "20899048083289")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(File.ReadAllLines(".\\Day20\\test_input.txt"), "273")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var imageArray = ParseTiles(input);

            var corners = imageArray.FindCorners();
            var result = corners.Aggregate(1L, (acc, t) => acc *= t.Id);
                        
            return result.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var pattern = new[]
            {
                "                  # ",
                "#    ##    ##    ###",
                " #  #  #  #  #  #   ",
            };
            var imageArray = ParseTiles(input);

            var image = imageArray.RearrangeImage();

            // Trying all rotation and flipping
            var finder = new PatternFinder(image);
            var count = 0;
            for (int i = 0; i < 4; i++)
            {
                count = Math.Max(count, finder.CountPattern(pattern));
                image.RotateLeft();
            }

            image.FlipHorizontally();
            for (int i = 0; i < 4; i++)
            {
                count = Math.Max(count, finder.CountPattern(pattern));
                image.RotateLeft();
            }

            var patternBlackPixelCount = pattern.Sum(r => r.Count(p => p == '#'));
            var roughness = image.BlackPixelCount - count * patternBlackPixelCount;
            return roughness.ToString();
        }

        private ImageArray ParseTiles(IEnumerable<string> input)
        {
            var tiles = new List<ImageTile>();
            var id = 0;
            var piece = new List<string>();
            foreach (var line in input.Append(string.Empty))
            {
                if (line.StartsWith("Tile"))
                {
                    id = int.Parse(line.Split(' ')[1].Trim(':'));
                    piece.Clear();
                }
                else if (line == string.Empty)
                {
                    tiles.Add(new ImageTile(id, new PixelMatrix(piece.ToArray())));
                }
                else
                {
                    piece.Add(line);
                }
            }

            return new ImageArray(tiles);
        }
    }
}
