﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day20
{
    class ImageBuilder
    {
        private readonly List<List<char>> pixels = new List<List<char>>();

        public void AddPixel(char value, (int X, int Y) coordinate)
        {
            if (coordinate.Y > pixels.Count)
            {
                throw new InvalidOperationException($"Y is out of range {coordinate.Y}");
            }

            if (coordinate.Y == pixels.Count)
            {
                pixels.Add(new List<char>());
            }

            if (coordinate.X > pixels[coordinate.Y].Count)
            {
                throw new InvalidOperationException($"X is out of range {coordinate.X}");
            }

            pixels[coordinate.Y].Add(value);
        }

        public PixelMatrix Build()
        {
            var data = pixels.Select(r => string.Join("", r)).ToArray();

            return new PixelMatrix(data);
        }
    }
}
