﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day20
{
    class Border : IEnumerable<char>
    {
        private readonly PixelMatrix image;
        private readonly (int X, int Y) start;
        private readonly (int X, int Y) increment;
        private readonly (int X, int Y) end;

        public Border(PixelMatrix image, (int X, int Y) start, (int X, int Y) increment, (int X, int Y) end)
        {
            this.image = image;
            this.start = start;
            this.increment = increment;
            this.end = end;

            Id = new DynamicBorderId(this);
        }

        internal DynamicBorderId Id { get; }

        public bool IsEdge(IEnumerable<IBorderId> edgeIds)
        {
            return edgeIds.Any(i => i.Equals(Id));
        }

        public bool IsMatch(Border other)
        {
            return Id.Equals(other.Id);
        }

        public bool IsAligned(Border other)
        {
            return Id.ReverseEquals(other.Id);
        }

        public IEnumerator<char> GetEnumerator()
        {
            return new BorderIterator(image, start, increment, end);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new BorderIterator(image, start, increment, end);
        }
    }
}
