﻿using System.Collections;
using System.Collections.Generic;

namespace AdventOfCode2020.Day20
{
    class BorderIterator : IEnumerator<char>
    {
        private readonly (int, int) RESET = (-1, -1);
        private readonly PixelMatrix image;
        private readonly (int X, int Y) start;
        private (int X, int Y) current;
        private readonly (int X, int Y) increment;
        private readonly (int X, int Y) end;

        public BorderIterator(PixelMatrix image, (int X, int Y) start, (int X, int Y) increment, (int X, int Y) end)
        {
            current = RESET;
            this.image = image;
            this.start = start;
            this.increment = increment;
            this.end = end;
        }

        public char Current => image.GetPixel(current);

        object IEnumerator.Current => image.GetPixel(start);

        public bool MoveNext()
        {
            if (current == end)
            {
                return false;
            }
            if (current == RESET)
            {
                current = start;
                return true;
            }

            current.X += increment.X;
            current.Y += increment.Y;
            return true;
        }

        public void Reset()
        {
            current = RESET;
        }

        public void Dispose()
        {
            // Noop
        }
    }
}
