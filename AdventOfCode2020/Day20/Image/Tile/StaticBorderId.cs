﻿namespace AdventOfCode2020.Day20
{
    class StaticBorderId : IBorderId
    {
        public StaticBorderId(IBorderId other)
        {
            Id = other.Id;
            ReverseId = other.ReverseId;
        }

        public int Id { get; }

        public int ReverseId { get; }

        public bool Equals(IBorderId other)
        {
            return other.Id == Id || other.Id == ReverseId;
        }

        public bool ReverseEquals(IBorderId other)
        {
            return other.Id == Id;
        }

        public override string? ToString()
        {
            return $"{Id}:{ReverseId}";
        }
    }
}
