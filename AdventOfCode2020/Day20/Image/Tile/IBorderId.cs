﻿using System;

namespace AdventOfCode2020.Day20
{
    interface IBorderId : IEquatable<IBorderId>
    {
        int Id { get; }
        int ReverseId { get; }

        bool ReverseEquals(IBorderId other);
    }
}
