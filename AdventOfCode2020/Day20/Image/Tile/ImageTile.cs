﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace AdventOfCode2020.Day20
{
    [DebuggerDisplay("{Id}")]
    class ImageTile
    {
        private readonly Border[] borders;
        private readonly PixelMatrix image;

        public ImageTile(int id, PixelMatrix image)
        {
            Id = id;
            this.image = image;

            TopBorder = new Border(image, (0, 0), (1, 0), (image.Width - 1, 0));
            RightBorder = new Border(image, (image.Width - 1, 0), (0, 1), (image.Width - 1, image.Height - 1));
            BottomBorder = new Border(image, (image.Width - 1, image.Height - 1), (-1, 0), (0, image.Height - 1));
            LeftBorder = new Border(image, (0, image.Height - 1), (0, -1), (0, 0));

            borders = new[] { TopBorder, RightBorder, BottomBorder, LeftBorder };
        }

        public long Id { get; }
        public Border RightBorder { get; }
        public Border LeftBorder { get; }
        public Border BottomBorder { get; }

        public Border TopBorder { get; }

        public IEnumerable<DynamicBorderId> GetBorderIds()
        {
            return borders.Select(b => b.Id);
        }

        public bool IsCorner(IEnumerable<IBorderId> edgeIds)
        {
            var numberOfEdges = borders.Count(e => e.IsEdge(edgeIds));
            return numberOfEdges == 2;
        }

        public bool HasMatchingBorder(Border border)
        {
            return borders.Any(b => b.IsMatch(border));
        }

        public void FlipHorizontally()
        {
            image.FlipHorizontally();
        }

        public void FlipVertically()
        {
            image.FlipVertically();
        }

        public void RotateLeft()
        {
            image.RotateLeft();
        }

        public void ExportImage(ImageBuilder builder, int column, int row)
        {
            // +1 and -1 for the "borders" around the tiles
            for(int y = 1; y < image.Height - 1; y++)
            {
                for (int x = 1; x < image.Width - 1; x++)
                {
                    // -2 for the "borders" around the tiles
                    var rowIndex = row * (image.Height - 2) + y - 1;
                    var columnIndex = column * (image.Width - 2) + x - 1;
                    builder.AddPixel(image.GetPixel((x, y)), (columnIndex, rowIndex));
                }
            }
        }

        internal void Print()
        {
            image.Print();
        }
    }
}
