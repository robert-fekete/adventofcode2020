﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day20
{
    class DynamicBorderId : IBorderId
    {
        private readonly Border border;

        public DynamicBorderId(Border border)
        {
            this.border = border;

            if(Id == ReverseId)
            {
                throw new NotImplementedException("Can't handle symmetric borders");
            }
        }

        public int Id => CalculateId(border);
        public int ReverseId => CalculateId(border.Reverse());

        public bool Equals(IBorderId other)
        {
            return other.Id == Id || other.Id == ReverseId;
        }

        public bool ReverseEquals(IBorderId other)
        {
            return other.Id == ReverseId;
        }

        private int CalculateId(IEnumerable<char> border)
        {
            var id = 0;
            foreach(var c in border)
            {
                id <<= 1;
                id |= (c == '#' ? 1 : 0);
            }

            return id;
        }

        public override string? ToString()
        {
            return $"{Id}:{ReverseId}";
        }
    }
}
