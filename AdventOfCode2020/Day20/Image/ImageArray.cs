﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day20
{
    class ImageArray
    {
        private readonly IReadOnlyCollection<ImageTile> tiles;

        public ImageArray(IReadOnlyCollection<ImageTile> tiles)
        {
            this.tiles = tiles;
        }

        public IReadOnlyCollection<ImageTile> FindCorners()
        {
            var edgeIds = GetEdgeIds();

            return FindCorners(tiles, edgeIds);
        }

        public PixelMatrix RearrangeImage()
        {
            var imageMerger = new ImageTileMerger();
            var remainingTiles = new List<ImageTile>(tiles);

            var edgeIds = GetEdgeIds();
            var firstTile = FindCorners(remainingTiles, edgeIds).First();
            remainingTiles.Remove(firstTile);

            while(!firstTile.TopBorder.IsEdge(edgeIds) || !firstTile.LeftBorder.IsEdge(edgeIds))
            {
                firstTile.RotateLeft();
            }

            var row = BuildRow(firstTile, remainingTiles, edgeIds);
            imageMerger.AddRow(row);

            while (remainingTiles.Any())
            {
                var previousStartTile = imageMerger.GetLastStartTile();
                var currentTile = FindNextStartTile(previousStartTile, remainingTiles);

                row = BuildRow(currentTile, remainingTiles, edgeIds);
                imageMerger.AddRow(row);
            }

            return imageMerger.GetImage();
        }

        private ImageTile FindNextStartTile(ImageTile previousFirstTile, List<ImageTile> remainingTiles)
        {
            foreach (var tile in remainingTiles)
            {
                if (tile.HasMatchingBorder(previousFirstTile.BottomBorder))
                {
                    while (!tile.TopBorder.IsMatch(previousFirstTile.BottomBorder))
                    {
                        tile.RotateLeft();
                    }

                    if (!tile.TopBorder.IsAligned(previousFirstTile.BottomBorder))
                    {
                        tile.FlipHorizontally();
                    }

                    remainingTiles.Remove(tile);
                    return tile;
                }
            }

            throw new InvalidOperationException("Couldn't find the next starter tile");
        }

        private static List<ImageTile> BuildRow(ImageTile currentTile, List<ImageTile> remainingTiles, IReadOnlyCollection<IBorderId> edgeIds)
        {
            var row = new List<ImageTile>() { currentTile };
            while (!currentTile.RightBorder.IsEdge(edgeIds))
            {
                foreach (var tile in remainingTiles)
                {
                    if (tile.HasMatchingBorder(currentTile.RightBorder))
                    {
                        while (!tile.LeftBorder.IsMatch(currentTile.RightBorder))
                        {
                            tile.RotateLeft();
                        }

                        if (!tile.LeftBorder.IsAligned(currentTile.RightBorder))
                        {
                            tile.FlipVertically();
                        }

                        row.Add(tile);
                        remainingTiles.Remove(tile);
                        currentTile = tile;
                        break;
                    }
                }
            }

            return row;
        }

        private IReadOnlyCollection<IBorderId> GetEdgeIds()
        {
            var borderIds = new Dictionary<IBorderId, int>();
            var bb = tiles.SelectMany(t => t.GetBorderIds());
            foreach (var borderId in bb)
            {
                var matchingIds = borderIds.Keys.Where(b => b.Equals(borderId));
                if (matchingIds.Any())
                {
                    var key = matchingIds.First();
                    borderIds[key]++;
                }
                else
                {
                    borderIds[borderId] = 1;
                }
            }

            var edgeIds = borderIds.Where(kvp => kvp.Value == 1)
                                   .Select(kvp => new StaticBorderId(kvp.Key))
                                   .ToArray<IBorderId>();
            return edgeIds;
        }

        private IReadOnlyCollection<ImageTile> FindCorners(IReadOnlyCollection<ImageTile> tiles, IReadOnlyCollection<IBorderId> edgeIds)
        {
            return tiles.Where(t => t.IsCorner(edgeIds)).ToArray();
        }
    }
}
