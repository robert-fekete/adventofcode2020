﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day20
{
    class ImageTileMerger
    {
        private readonly List<List<ImageTile>> imageTiles = new List<List<ImageTile>>();

        public void AddRow(IReadOnlyCollection<ImageTile> row)
        {
            imageTiles.Add(new List<ImageTile>(row));
        }

        public ImageTile GetLastStartTile()
        {
            return imageTiles.Last()[0];
        }

        public PixelMatrix GetImage()
        {
            var builder = new ImageBuilder();
            for(int y = 0; y < imageTiles.Count; y++)
            {
                for(int x = 0; x < imageTiles[0].Count; x++)
                {
                    imageTiles[y][x].ExportImage(builder, x, y);
                }
            }

            return builder.Build();
        }
    }
}
