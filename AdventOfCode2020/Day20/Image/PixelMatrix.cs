﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day20
{
    class PixelMatrix
    {
        private string[] data;

        public PixelMatrix(string[] data)
        {
            this.data = data;
        }

        public int Width => data[0].Length;
        public int Height => data.Length;
        public int BlackPixelCount => data.Sum(r => r.Count(p => p == '#'));

        internal char GetPixel((int X, int Y) coordinate)
        {
            return data[coordinate.Y][coordinate.X];
        }

        internal void FlipVertically()
        {
            data = data.Reverse().ToArray();
        }

        internal void FlipHorizontally()
        {
            data = data.Select(r => string.Join("", r.Reverse())).ToArray();
        }

        internal void RotateLeft()
        {
            var newData = new List<string>();
            for(int x = Width - 1; x >= 0; x--)
            {
                var row = new List<char>();
                for(int y = 0; y < Height; y++)
                {
                    row.Add(data[y][x]);
                }
                newData.Add(string.Join("", row));
            }

            data = newData.ToArray();
        }

        internal void Print()
        {
            foreach(var row in data)
            {
                Console.WriteLine(row);
            }
            Console.WriteLine();
        }
    }
}
