﻿using AdventOfCode.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day5
{
    class Solver : ISolver
    {
        public int DayNumber => 5;

        public string FirstExpected => "890";

        public string SecondExpected => "651";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase("FBFBBFFRLR", 357)
                .AddTestCase("BFFFBBFRRR", 567)
                .AddTestCase("FFFBBBFRRR", 119)
                .AddTestCase("BBFFBBFRLL", 820)
                .AddTest(CalculateSeatId)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return input.Max(CalculateSeatId).ToString();
        }

        private int CalculateSeatId(string seat)
        {
            // return Convert.ToInt32(seat.Replace("B", "1").Replace("R", "1").Replace("L", "0").Replace("F", "0"), 2);
            return seat.Select(l => l == 'B' || l == 'R' ? 1 : 0)
                       .Aggregate(0, (acc, bit) => (acc << 1) | bit);
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return FindSeat(input).ToString();
        }

        private int FindSeat(IEnumerable<string> input)
        {
            var seats = input.Select(CalculateSeatId);
            var n1 = seats.Min() - 1;
            var n2 = seats.Max();

            return (n2 * (n2 + 1) / 2) - (n1 * (n1 + 1) / 2) - seats.Sum();

            //var seats = input.Select(CalculateSeatId).OrderBy(s => s);
            //var prev = seats.First() - 1;
            //foreach (var seat in seats)
            //{
            //    if (prev + 1 != seat)
            //    {
            //        return seat - 1;
            //    }
            //    prev = seat;
            //}

            //throw new InvalidOperationException("Couldn't find the seat");
        }
    }
}
