﻿using System.Collections.Generic;

namespace AdventOfCode2020.Day14.Masks
{
    class NoopMask : IMask
    {
        public IReadOnlyCollection<long> Transform(long value) => new[] { value };
    }
}
