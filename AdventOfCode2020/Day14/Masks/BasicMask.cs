﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2020.Day14.Masks
{
    internal class BasicMask : IMask
    {
        private readonly List<Func<long, long>> transformations = new List<Func<long, long>>();

        public BasicMask()
        {
        }

        public BasicMask(string rawMask)
        {
            for (int i = 0; i < rawMask.Length; i++)
            {
                var index = rawMask.Length - 1 - i;
                var copy = i; // This copy is needed so the closures captures `i` by value instead of reference

                if (rawMask[index] == '1')
                {
                    transformations.Add(v => MaskToOne(v, copy));
                }
                else if (rawMask[index] == '0')
                {
                    transformations.Add(v => MaskToZero(v, copy));
                }
            }
        }

        public IReadOnlyCollection<long> Transform(long value)
        {   
            var newValue = value;
            foreach(var transformation in transformations)
            {
                newValue = transformation(newValue);
            }

            return new[] { newValue };
        }

        private long MaskToZero(long value, int position)
        {
            return value & ~(1L << position);
        }

        private long MaskToOne(long value, int position)
        {
            return value | (1L << position);
        }
    }
}