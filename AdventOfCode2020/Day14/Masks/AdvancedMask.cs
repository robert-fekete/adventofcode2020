﻿using System.Collections.Generic;

namespace AdventOfCode2020.Day14.Masks
{
    internal class AdvancedMask : IMask
    {
        private readonly string rawMask;

        public AdvancedMask()
        {
            rawMask = "000000000000000000000000000000000000";
        }

        public AdvancedMask(string rawMask)
        {
            this.rawMask = rawMask;
        }

        public IReadOnlyCollection<long> Transform(long originalValue)
        {
            var values = new List<long>();
            values.Add(originalValue);
            for (int i = 0; i < rawMask.Length; i++)
            {
                var index = rawMask.Length - 1 - i;
                values = GenerateNewValues(values, i, rawMask[index]);
            }

            return values;
        }

        private List<long> GenerateNewValues(List<long> values, int i, char bit)
        {
            var newValues = new List<long>();
            foreach (var value in values)
            {
                switch (bit)
                {
                    case '0':
                        newValues.Add(value);
                        break;
                    case '1':
                        var newValue = MaskToOne(value, i);
                        newValues.Add(newValue);
                        break;
                    case 'X':
                        var newValue1 = MaskToZero(value, i);
                        newValues.Add(newValue1);

                        var newValue2 = MaskToOne(value, i);
                        newValues.Add(newValue2);
                        break;
                }
            }
            values = newValues;
            return values;
        }

        private long MaskToZero(long value, int position)
        {
            return value & ~(1L << position);
        }

        private long MaskToOne(long value, int position)
        {
            return value | (1L << position);
        }
    }
}