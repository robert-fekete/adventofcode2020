﻿using System.Collections.Generic;

namespace AdventOfCode2020.Day14.Masks
{
    internal interface IMask
    {
        IReadOnlyCollection<long> Transform(long value);
    }
}