﻿using AdventOfCode.Framework;
using AdventOfCode2020.Day14.Instructions;
using AdventOfCode2020.Day14.Masks;
using System.Collections.Generic;

namespace AdventOfCode2020.Day14
{
    class Solver : ISolver
    {
        public int DayNumber => 14;

        public string FirstExpected => "15403588588538";

        public string SecondExpected => "3260587250457";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
                    "mem[8] = 11",
                    "mem[7] = 101",
                    "mem[8] = 0",
                }, "165")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "mask = 000000000000000000000000000000X1001X",
                    "mem[42] = 100",
                    "mask = 00000000000000000000000000000000X0XX",
                    "mem[26] = 1",
                }, "208")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var commands = ParseCommands(input);
            var visitor = new DecoderVisitor(new NoopMask(), new BasicMask(), _ => new NoopMask(), s => new BasicMask(s));
            var result = ExecuteCommands(visitor, commands);

            return result.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var commands = ParseCommands(input);
            var visitor = new DecoderVisitor(new AdvancedMask(), new NoopMask(), s => new AdvancedMask(s), _ => new NoopMask());
            var result = ExecuteCommands(visitor, commands);

            return result.ToString();
        }

        public long ExecuteCommands(DecoderVisitor decoder, IReadOnlyCollection<ICommand> commands)
        {
            foreach(var command in commands)
            {
                command.Visit(decoder);
            }

            return decoder.GetSum();
        }

        public IReadOnlyCollection<ICommand> ParseCommands(IEnumerable<string> input)
        {
            var commands = new List<ICommand>();
            foreach (var line in input)
            {
                if (line.StartsWith("mask"))
                {
                    var rawMask = line.Split('=')[1].Trim();
                    commands.Add(new UpdateMaskCommand(rawMask));
                }
                else if (line.StartsWith("mem"))
                {
                    var parts = line.Split('=');
                    var address = long.Parse(parts[0].Split('[')[1].Split(']')[0]);
                    var value = long.Parse(parts[1].Trim());

                    commands.Add(new WriteToMemoryCommand(address, value));
                }
            }

            return commands;
        }
    }
}
