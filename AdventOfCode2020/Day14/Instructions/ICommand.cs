﻿namespace AdventOfCode2020.Day14.Instructions
{
    interface ICommand
    {
        void Visit(DecoderVisitor visitor);
    }
}
