﻿using AdventOfCode2020.Day14.Masks;
using System;

namespace AdventOfCode2020.Day14.Instructions
{
    class UpdateMaskCommand : ICommand
    {
        private readonly string rawMask;

        public UpdateMaskCommand(string rawMask)
        {
            this.rawMask = rawMask;
        }

        public IMask GenerateMask(Func<string, IMask> maskFactory) => maskFactory(rawMask);

        public void Visit(DecoderVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
