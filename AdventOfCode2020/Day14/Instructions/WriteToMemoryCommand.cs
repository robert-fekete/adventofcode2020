﻿namespace AdventOfCode2020.Day14.Instructions
{
    class WriteToMemoryCommand : ICommand
    {
        public WriteToMemoryCommand(long address, long value)
        {
            Address = address;
            Value = value;
        }

        public long Address { get; }
        public long Value { get; }

        public void Visit(DecoderVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
