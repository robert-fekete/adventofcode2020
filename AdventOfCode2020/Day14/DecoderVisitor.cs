﻿using AdventOfCode2020.Day14.Masks;
using System;
using System.Linq;

namespace AdventOfCode2020.Day14.Instructions
{
    class DecoderVisitor
    {
        private readonly Func<string, IMask> addressMaskFactory;
        private readonly Func<string, IMask> valueMaskFactory;
        private readonly Memory memory = new Memory();

        private IMask addressMask;
        private IMask valueMask;

        public DecoderVisitor(IMask initialAddressMask, IMask initialValueMask, Func<string, IMask> addressMaskFactory, Func<string, IMask> valueMaskFactory)
        {
            addressMask = initialAddressMask;
            valueMask = initialValueMask;
            this.addressMaskFactory = addressMaskFactory;
            this.valueMaskFactory = valueMaskFactory;
        }

        internal void Visit(UpdateMaskCommand command)
        {
            addressMask = command.GenerateMask(addressMaskFactory);
            valueMask = command.GenerateMask(valueMaskFactory);
        }

        internal void Visit(WriteToMemoryCommand command)
        {
            var maskedAddresses = addressMask.Transform(command.Address);
            foreach (var maskedAddress in maskedAddresses)
            {
                var maskedValue = valueMask.Transform(command.Value).First();
                memory.Write(maskedAddress, maskedValue);
            }
        }

        internal long GetSum()
        {
            return memory.SumValues();
        }
    }
}