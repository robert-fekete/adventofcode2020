﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day14
{
    internal class Memory
    {
        private readonly Dictionary<long, long> values = new Dictionary<long, long>();

        internal void Write(long address, long value)
        {
            values[address] = value;
        }

        internal long SumValues()
        {
            return values.Sum(kvp => kvp.Value);
        }
    }
}