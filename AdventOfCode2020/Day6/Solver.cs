﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day6
{
    class Solver : ISolver
    {
        public int DayNumber => 6;

        public string FirstExpected => "6799";

        public string SecondExpected => "3354";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "abcx",
                    "abcy",
                    "abcz",
                }, "6")
                .AddTestCase(new[]
                {
                    "abc",
                    "",
                    "a",
                    "b",
                    "c",
                    "",
                    "ab",
                    "ac",
                    "",
                    "a",
                    "a",
                    "a",
                    "a",
                    "",
                    "b",
                }, "11")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "abcx",
                    "abcy",
                    "abcz",
                }, "3")
                .AddTestCase(new[]
                {
                    "abc",
                    "",
                    "a",
                    "b",
                    "c",
                    "",
                    "ab",
                    "ac",
                    "",
                    "a",
                    "a",
                    "a",
                    "a",
                    "",
                    "b",
                }, "6")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return ProcessPlane(input, (h, p) => h.UnionWith(p));
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return ProcessPlane(input, (h, p) => h.IntersectWith(p));
        }

        private string ProcessPlane(IEnumerable<string> input, Action<HashSet<char>, string> aggregator)
        {
            var sum = 0;
            var group = new List<string>();
            foreach (var line in input)
            {
                if (line == string.Empty)
                {
                    sum += ProcessGroup(group, aggregator);
                    group.Clear();
                }
                else
                {
                    group.Add(line);
                }
            }
            sum += ProcessGroup(group, aggregator);

            return sum.ToString();
        }

        public int ProcessGroup(IEnumerable<string> people, Action<HashSet<char>, string> aggregator)
        {
            var custom = new HashSet<char>(people.First());
            foreach(var person in people.Skip(1))
            {
                aggregator(custom, person);
            }

            return custom.Count;
        }
    }
}
