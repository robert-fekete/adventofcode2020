﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day25
{
    class Solver : ISolver
    {
        public int DayNumber => 25;

        public string FirstExpected => "";

        public string SecondExpected => "";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase((5764801L, 17807724L), 14897079L)
                .AddTest(GetEncryptionKey)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .Build(false);
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var a = long.Parse(input.First());
            var b = long.Parse(input.Skip(1).First());

            return GetEncryptionKey((a, b)).ToString();
        }

        private long GetEncryptionKey((long, long) publicKeys)
        {
            (var publicKey1, var publicKey2) = publicKeys;
            var accumulator = 1L;
            var loopSize = 0;

            while(accumulator != publicKey1)
            {
                accumulator *= 7;
                accumulator %= 20201227;
                loopSize++;
            }

            Console.WriteLine(loopSize);

            accumulator = 1L;
            for (int i = 0; i < loopSize; i++)
            {
                accumulator *= publicKey2;
                accumulator %= 20201227;
            }

            return accumulator;
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            throw new NotImplementedException();
        }
    }
}
