﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AdventOfCode2020.Day19
{
    class Solver : ISolver
    {
        public int DayNumber => 19;

        public string FirstExpected => "216";

        public string SecondExpected => "400";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "0: 4 1 5",      
                    "1: 2 3 | 3 2",
                    "2: 4 4 | 5 5",
                    "3: 4 5 | 5 4",
                    "4: \"a\"",
                    "5: \"b\"",
                    "",
                    "ababbb",
                    "bababa",
                    "abbbab",
                    "aaabbb",
                    "aaaabbb"
                }, "2")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "42: 9 14 | 10 1",
                    "9: 14 27 | 1 26",
                    "10: 23 14 | 28 1",
                    "1: \"a\"",
                    "11: 42 31",
                    "5: 1 14 | 15 1",
                    "19: 14 1 | 14 14",
                    "12: 24 14 | 19 1",
                    "16: 15 1 | 14 14",
                    "31: 14 17 | 1 13",
                    "6: 14 14 | 1 14",
                    "2: 1 24 | 14 4",
                    "0: 8 11",
                    "13: 14 3 | 1 12",
                    "15: 1 | 14",
                    "17: 14 2 | 1 7",
                    "23: 25 1 | 22 14",
                    "28: 16 1",
                    "4: 1 1",
                    "20: 14 14 | 1 15",
                    "3: 5 14 | 16 1",
                    "27: 1 6 | 14 18",
                    "14: \"b\"",
                    "21: 14 1 | 1 14",
                    "25: 1 1 | 1 14",
                    "22: 14 14",
                    "8: 42",
                    "26: 14 22 | 1 20",
                    "18: 15 15",
                    "7: 14 5 | 1 21",
                    "24: 14 1",
                    "",
                    "abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa",
                    "bbabbbbaabaabba",
                    "babbbbaabbbbbabbbbbbaabaaabaaa",
                    "aaabbbbbbaaaabaababaabababbabaaabbababababaaa",
                    "bbbbbbbaaaabbbbaaabbabaaa",
                    "bbbababbbbaaaaaaaabbababaaababaabab",
                    "ababaaaaaabaaab",
                    "ababaaaaabbbaba",
                    "baabbaaaabbaaaababbaababb",
                    "abbbbabbbbaaaababbbbbbaaaababb",
                    "aaaaabbaabaaaaababaa",
                    "aaaabbaaaabbaaa",
                    "aaaabbaabbaaaaaaabbbabbbaaabbaabaaa",
                    "babaaabbbaaabaababbaabababaaab",
                    "aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba",
                }, "12")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            // Parse
            (var messages, var rules, var dependencies) = ParseInput(input);

            // Normalizing
            var sorted = TopologicalSort(dependencies, 0);

            // Regex building
            var regexParts = BuildRegexes(rules, sorted, false);

            // Matching
            var regex = new Regex($"^{regexParts[0]}$");
            var matchCount = messages.Count(s => regex.IsMatch(s));

            return matchCount.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            // Parse
            (var messages, var rules, var dependencies) = ParseInput(input);

            // Normalizing
            var sorted = TopologicalSort(dependencies, 0);

            // Regex building
            var regexParts = BuildRegexes(rules, sorted, true);

            // Matching
            var regex = new Regex($"^{regexParts[0]}$");
            var matchCount = messages.Count(s => regex.IsMatch(s));

            return matchCount.ToString();
        }

        private static Dictionary<int, string> BuildRegexes(Dictionary<int, string> rules, IReadOnlyCollection<int> sorted, bool isSecond)
        {
            var regexParts = new Dictionary<int, string>();
            foreach (var id in sorted)
            {
                var rule = rules[id];

                if (rule.Contains('"'))
                {
                    var match = rule.Replace("\"", "");
                    regexParts[id] = match;
                }
                else
                {
                    var builder = new StringBuilder();
                    builder.Append("((");
                    if (isSecond && id == 8)
                    {
                        builder.Append($"{regexParts[42]}+");
                    }
                    else if (isSecond && id == 11)
                    {
                        var token = $"({regexParts[42]}{regexParts[31]})";
                        builder.Append(token);
                        for (int i = 0; i < 4; i++)
                        {
                            token = $"({regexParts[42]}{token}{regexParts[31]})";
                            builder.Append($"|{token}");
                        }
                    }
                    else {
                        foreach (var part in rule.Split(' '))
                        {
                            if (part == "|")
                            {
                                builder.Append(")|(");
                            }
                            else
                            {
                                var otherId = int.Parse(part);
                                builder.Append(regexParts[otherId]);
                            }
                        }
                    }
                    builder.Append("))");

                    regexParts[id] = builder.ToString();
                }
            }

            return regexParts;
        }

        private (List<string>, Dictionary<int, string>, Dictionary<int, List<int>>) ParseInput(IEnumerable<string> input)
        {
            var messages = new List<string>();
            var rules = new Dictionary<int, string>();
            var dependencies = new Dictionary<int, List<int>>();
            var hasEmpty = false;
            foreach (var line in input)
            {
                if (line == string.Empty)
                {
                    hasEmpty = true;
                    continue;
                }
                if (!hasEmpty)
                {
                    var parts = line.Split(':');
                    var id = int.Parse(parts[0]);

                    var rule = parts[1].Trim();
                    rules[id] = rule;
                    var ruleDependencies = rule.Split(' ').Where(p => int.TryParse(p, out var _)).Select(int.Parse).Distinct().ToList();
                    dependencies[id] = ruleDependencies;
                }
                else
                {
                    messages.Add(line);
                }
            }

            return (messages, rules, dependencies);
        }

        private IReadOnlyCollection<int> TopologicalSort(Dictionary<int, List<int>> neighbours, int startingNode)
        {
            var backlog = new Stack<int>();
            var visited = new HashSet<int>();

            TopologicalSortRec(neighbours, startingNode, backlog, visited);

            return backlog.Reverse().ToArray();
        }

        private void TopologicalSortRec(Dictionary<int, List<int>> neighbours, int node, Stack<int> backlog, HashSet<int> visited)
        {
            visited.Add(node);

            foreach (var next in neighbours[node])
            {
                if (!visited.Contains(next))
                {
                    TopologicalSortRec(neighbours, next, backlog, visited);
                }
            }

            backlog.Push(node);
        }
    }
}
