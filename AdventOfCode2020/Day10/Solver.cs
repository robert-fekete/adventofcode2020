﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day10
{
    class Solver : ISolver
    {
        public int DayNumber => 10;

        public string FirstExpected => "2450";

        public string SecondExpected => "32396521357312";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    16,
                    10,
                    15,
                    5,
                    1,
                    11,
                    7,
                    19,
                    6,
                    12,
                    4,
                }, (7, 5))
                .AddTest(CalculateJotageDifferences)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            {
                return new TestBuilder()
                    .AddTestCase(new[]
                    {
                        "16",
                        "10",
                        "15",
                        "5",
                        "1",
                        "11",
                        "7",
                        "19",
                        "6",
                        "12",
                        "4",
                    }, "8")
                    .AddTestCase(new[]
                    {
                        "28",
                        "33",
                        "18",
                        "42",
                        "31",
                        "14",
                        "46",
                        "20",
                        "48",
                        "47",
                        "24",
                        "23",
                        "49",
                        "45",
                        "19",
                        "38",
                        "39",
                        "11",
                        "1",
                        "32",
                        "25",
                        "35",
                        "8",
                        "17",
                        "7",
                        "9",
                        "4",
                        "2",
                        "34",
                        "10",
                        "3",
                    }, "19208")
                    .AddTest(ExecuteSecond)
                    .Build();
            }
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            (var ones, var threes) = CalculateJotageDifferences(input.Select(int.Parse));

            return (ones * threes).ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var numbers = input.Select(int.Parse).OrderBy(j => j).ToArray();
            var max = numbers.Max();

            return CountWays(numbers, max).ToString();
        }

        private (int, int) CalculateJotageDifferences(IEnumerable<int> joltages)
        {
            var diffs = new Dictionary<int, int>()
            {
                { 1, 0 },
                { 2, 0 },
                { 3, 0 },
            };

            var previous = 0;
            foreach (var joltage in joltages.OrderBy(j => j))
            {
                var diff = joltage - previous;
                diffs[diff]++;

                previous = joltage;
            }

            return (diffs[1], diffs[3] + 1); // +1 for the device's built-in adapter
        }


        private long CountWays(int[] numbers, int target)
        {
            var dp = new Dictionary<int, long>();
            for (int i = -3; i <= target; i++)
            {
                dp[i] = 0;
            }

            dp[0] = 1;
            foreach(var number in numbers)
            {
                dp[number] = dp[number - 1] + dp[number - 2] + dp[number - 3];
            }

            return dp[target];
        }
    }
}
