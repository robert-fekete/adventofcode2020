﻿namespace AdventOfCode2020.Day15
{
    class SmartArray
    {
        private const int OFFSET = 1;
        private readonly int[] numbers;

        public SmartArray(int length)
        {
            numbers = InitNumbers(length);
        }

        private int[] InitNumbers(int length)
        {
            var array = new int[length];
            for (int i = 0; i < length; i++)
            {
                array[i] = 0;
            }

            return array;
        }

        public bool IsZero(int index) => numbers[index] == 0;

        public int Read(int index) => numbers[index] - OFFSET;

        public void Add(int index, int value)
        {
            numbers[index] = value + OFFSET;
        }
    }
}
