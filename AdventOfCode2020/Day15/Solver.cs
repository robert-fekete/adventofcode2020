﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day15
{
    class Solver : ISolver
    {
        public int DayNumber => 15;

        public string FirstExpected => "475";

        public string SecondExpected => "11261";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[] { 0, 3, 6 }, 2020, 436)
                .AddTestCase(new[] { 1, 3, 2 }, 2020, 1)
                .AddTestCase(new[] { 2, 1, 3 }, 2020, 10)
                .AddTestCase(new[] { 1, 2, 3 }, 2020, 27)
                .AddTest(PlayMemoryGame)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .Build(true);
                //.AddTestCase(new[] { 0, 3, 6 }, 30000000, 175594)
                //.AddTestCase(new[] { 1, 3, 2 }, 30000000, 2578)
                //.AddTestCase(new[] { 2, 1, 3 }, 30000000, 3544142)
                //.AddTest(PlayMemoryGame)
                //.Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var sequence = input.First().Split(',').Select(int.Parse);
            return PlayMemoryGame(sequence, 2020).ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var sequence = input.First().Split(',').Select(int.Parse);
            return PlayMemoryGame(sequence, 30000000).ToString();
        }

        private int PlayMemoryGame(IEnumerable<int> startingSequence, int length)
        {
            var numbers = new SmartArray(length);
            var index = 0;
            foreach(var number in startingSequence.SkipLast(1))
            {
                numbers.Add(number, index);
                index++;
            }

            int lastNumber = startingSequence.Last();
            while (index + 1 < length)
            {
                var number = 0;
                if (!numbers.IsZero(lastNumber))
                {
                    number = index - numbers.Read(lastNumber);
                }

                numbers.Add(lastNumber, index);
                lastNumber = number;
                
                index++;
            }

            return lastNumber;
        }

        private int FindLastMatchingIndex(IList<int> numbers, int targetNumber)
        {
            for (int i = numbers.Count - 1; i >= 0; i--)
            {
                if (numbers[i] == targetNumber)
                {
                    return i;
                }
            }

            throw new InvalidOperationException("Couldn't find target");
        }
    }
}
