﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day4
{
    class Solver : ISolver
    {
        public int DayNumber => 4;

        public string FirstExpected => "264";

        public string SecondExpected => "224";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd",
                    "byr:1937 iyr:2017 cid:147 hgt:183cm",
                    "",
                    "hcl:#ae17e1 iyr:2013",
                    "eyr:2024",
                    "ecl:brn pid:760753108 byr:1931",
                    "hgt:179cm",
                    "",
                    "hcl:#cfa07d eyr:2025 pid:166559648",
                    "iyr:2011 ecl:brn hgt:59in",
                    "",
                    "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884",
                    "hcl:#cfa07d byr:1929",
                }
                , "2")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase("2002", "byr", true)
                .AddTestCase("2003", "byr", false)
                .AddTestCase("60in", "hgt", true)
                .AddTestCase("190cm", "hgt", true)
                .AddTestCase("190in", "hgt", false)
                .AddTestCase("190", "hgt", false)
                .AddTestCase("#123abc", "hcl", true)
                .AddTestCase("#123abz", "hcl", false)
                .AddTestCase("123abc", "hcl", false)
                .AddTestCase("brn", "ecl", true)
                .AddTestCase("wat", "ecl", false)
                .AddTestCase("000000001", "pid", true)
                .AddTestCase("1023456789", "pid", false)
                .AddTest((i, p) => ValidateField(p, i))
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var passports = ParsePassports(input);

            return passports.Count(p => ValidateRequiredFields(p)).ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var passports = ParsePassports(input);

            return passports.Count(p => ValidateRequiredFields(p) && ValidateFieldValues(p)).ToString();
        }

        private bool ValidateRequiredFields(Dictionary<string, string> passport)
        {
            var requiredFields = new[] { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" };
            var keys = passport.Keys;
            return requiredFields.All(keys.Contains);
        }

        private bool ValidateFieldValues(Dictionary<string, string> passport)
        {
            return passport.All(kvp => ValidateField(kvp.Key, kvp.Value));
        }

        private bool ValidateField(string key, string value)
        {
            var validEyeColors = new[] { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" };
            switch (key)
            {
                case "byr":
                    return ValidateYear(value, 1920, 2002);
                case "iyr":
                    return ValidateYear(value, 2010, 2020);
                case "eyr":
                    return ValidateYear(value, 2020, 2030);
                case "hgt":
                    return ValidateHeight(value, "in", 59, 76) || ValidateHeight(value, "cm", 150, 193);
                case "hcl":
                    return value.Length == 7 && value[0] == '#' && value.Substring(1, 6).All(IsHex);
                case "ecl":
                    return validEyeColors.Contains(value);
                case "pid":
                    return value.Length == 9 && value.All(char.IsDigit);
                case "cid":
                    return true;
            }

            return false;
        }

        private static bool ValidateHeight(string value, string unit, int from, int to)
        {
            if (value.EndsWith(unit))
            {
                var height = int.Parse(value.Substring(0, value.Length - 2));
                return from <= height && height <= to;
            }
            return false;
        }

        private static bool ValidateYear(string value, int from, int to)
        {
            if (value.Length == 4 && value.All(char.IsDigit))
            {
                var year = int.Parse(value);
                return year >= from && year <= to;
            }
            return false;
        }

        private bool IsHex(char arg) => char.IsDigit(arg) || ('a' <= arg && arg <= 'f');

        private IReadOnlyCollection<Dictionary<string, string>> ParsePassports(IEnumerable<string> input)
        {
            var passports = new List<Dictionary<string, string>>();
            var currentPassport = new Dictionary<string, string>();
            foreach (var line in input)
            {
                if (line == string.Empty)
                {
                    passports.Add(currentPassport);
                    currentPassport = new Dictionary<string, string>();
                }
                else
                {
                    var parts = line.Split(' ');
                    foreach (var part in parts)
                    {
                        var fields = part.Split(':');
                        currentPassport[fields[0]] = fields[1];
                    }
                }
            }
            passports.Add(currentPassport);

            return passports;
        }
    }
}
