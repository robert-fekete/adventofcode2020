﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day18
{
    class Solver : ISolver
    {
        public int DayNumber => 18;

        public string FirstExpected => "2743012121210";

        public string SecondExpected => "65658760783597";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase("2 * 3 + (4 * 5)", 26L)
                .AddTestCase("5 + (8 * 3 + 9 + 3 * 4 * 3)", 437L)
                .AddTestCase("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 12240L)
                .AddTestCase("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 13632L)
                .AddTest(EvaluateExpression)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase("1 + (2 * 3) + (4 * (5 + 6))", 51L)
                .AddTestCase("2 * 3 + (4 * 5)", 46L)
                .AddTestCase("5 + (8 * 3 + 9 + 3 * 4 * 3)", 1445L)
                .AddTestCase("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 669060L)
                .AddTestCase("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 23340L)
                .AddTest(EvaluateExpressionRec)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return input.Sum(EvaluateExpression).ToString();
        }

        private long EvaluateExpression(string expression)
        {
            var stack = new Stack<(long, Func<long, long, long>)>();
            var iter = 0;
            var result = 0L;
            Func<long, long, long> op = (a, b) => b;
            while (iter < expression.Length)
            {
                if (expression[iter] == '(')
                {
                    stack.Push((result, op));
                    op = (a, b) => b;
                }
                else if (expression[iter] == ')')
                {
                    var prev = stack.Pop();
                    op = prev.Item2;
                    result = op(prev.Item1, result);
                }
                else if (expression[iter] == ' ')
                {
                    // skip
                }
                else if (expression[iter] == '*')
                {
                    op = (a, b) => a * b;
                }
                else if (expression[iter] == '+')
                {
                    op = (a, b) => a + b;
                }
                else
                {
                    var value = int.Parse(expression.Substring(iter, 1));
                    result = op(result, value);
                }

                iter++;
            }

            return result;
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return input.Sum(EvaluateExpressionRec).ToString();
        }

        private long EvaluateExpressionRec(string rawExpression)
        {
            var expression = rawExpression.Replace(" ", "");
            var stack = new Stack<(long Operandus, Func<long, long, long> Operator)>();
            var iter = 0;
            var operandus = 0L;
            Func<long, long, long> currentOperator = (a, b) => b;
            while (iter < expression.Length)
            {
                if (expression[iter] == '(')
                {   
                    (var value, var endIndex) = EvaluateParenthesis(expression.Substring(iter));
                    iter += endIndex;
                    operandus = currentOperator(operandus, value);
                }
                else if (expression[iter] == '*')
                {
                    currentOperator = (a, b) => a * b;
                    stack.Push((operandus, currentOperator));
                    currentOperator = (a, b) => b;
                    operandus = 0;

                }
                else if (expression[iter] == '+')
                {
                    currentOperator = (a, b) => a + b;
                }
                else
                {
                    var value = int.Parse(expression.Substring(iter, 1));
                    operandus = currentOperator(operandus, value);
                }

                iter++;
            }

            while (stack.Any())
            {
                (var value, var @operator) = stack.Pop();
                operandus = @operator(value, operandus);
            }

            return operandus;
        }

        private (long, int) EvaluateParenthesis(string expression)
        {
            var opening = 0;
            var length = 0;
            var level = 0;
            for (int i = 0; i < expression.Length; i++)
            {
                if (expression[i] == '(')
                {
                    if (level == 0)
                    {
                        opening = i + 1;
                    }
                    level++;
                }
                else if (expression[i] == ')')
                {
                    level--;
                    if (level == 0)
                    {
                        length = i - opening;
                        break;
                    }
                }
            }
            var subExpression = expression.Substring(opening, length);
            return (EvaluateExpressionRec(subExpression), opening + length);
        }
    }
}
