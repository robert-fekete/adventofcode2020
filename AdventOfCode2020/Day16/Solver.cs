﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day16
{
    class Solver : ISolver
    {
        public int DayNumber => 16;

        public string FirstExpected => "23054";

        public string SecondExpected => "51240700105297";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "class: 1-3 or 5-7",
                    "row: 6-11 or 33-44",
                    "seat: 13-40 or 45-50",
                    "",
                    "your ticket:",
                    "7,1,14",
                    "",
                    "nearby tickets:",
                    "7,3,47",
                    "40,4,50",
                    "55,2,20",
                    "38,6,12",
                }, "71")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "class: 0-1 or 4-19",
                    "row: 0-5 or 8-19",
                    "seat: 0-13 or 16-19",
                    "",
                    "your ticket:",
                    "11,12,13",
                    "",
                    "nearby tickets:",
                    "3,9,18",
                    "15,1,5",
                    "5,14,9",
                }, new[] { "class", "row", "seat" }, 1716L)
                .AddTest(SolveSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var fields = ParseFields(input);
            var otherTickets = ParseOtherTickets(input);

            var scanningError = 0;
            foreach(var ticket in otherTickets)
            {
                foreach (var field in ticket)
                {
                    if(fields.Values.All(r => !r.IsValid(field)))
                    {
                        scanningError += field;
                    }
                }
            }

            return scanningError.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var fields = ParseFields(input);

            var result = SolveSecond(input, fields.Keys.Where(n => n.StartsWith("departure")));
            return result.ToString();
        }

        private long SolveSecond(IEnumerable<string> input, IEnumerable<string> targetFieldNames)
        {
            var fields = ParseFields(input);
            var yourTicket = ParseYourTicket(input);
            var otherTickets = ParseOtherTickets(input).Where(t => IsValidTicket(t, fields));

            var indexToFieldMap = new Dictionary<int, List<string>>();
            for (int i = 0; i < yourTicket.Length; i++)
            {
                indexToFieldMap[i] = fields.Keys.ToList();
            }

            foreach(var ticket in otherTickets)
            {
                for(int i = 0; i < ticket.Length; i++)
                {
                    var field = ticket[i];
                    foreach(var kvp in fields)
                    {
                        var fieldName = kvp.Key;
                        var fieldRule = kvp.Value;
                        if (!fieldRule.IsValid(field))
                        {
                            indexToFieldMap[i].Remove(fieldName);
                        }
                    }
                }
            }

            // Reduce
            var done = new HashSet<int>();
            while(!indexToFieldMap.Values.All(v => v.Count == 1))
            {
                foreach(var kvp in indexToFieldMap)
                {
                    if (done.Contains(kvp.Key))
                    {
                        continue;
                    }
                    if (kvp.Value.Count == 1)
                    {
                        foreach(var kvp2 in indexToFieldMap)
                        {
                            if (kvp2.Key != kvp.Key)
                            {
                                kvp2.Value.Remove(kvp.Value[0]);
                            }
                        }
                        done.Add(kvp.Key);
                    }
                }
            }

            var result = 1L;
            foreach(var fieldName in targetFieldNames)
            {
                var index = indexToFieldMap.First(kvp => kvp.Value.Contains(fieldName)).Key;
                result *= yourTicket[index];
            }

            return result;
        }

        private bool IsValidTicket(int[] ticket, Dictionary<string, Rule> fields)
        {
            return ticket.All(f => fields.Values.Any(r => r.IsValid(f)));
        }

        private Dictionary<string, Rule> ParseFields(IEnumerable<string> input)
        {
            var fields = new Dictionary<string, Rule>();
            foreach (var line in input)
            {
                if (line == string.Empty)
                {
                    return fields;
                }

                var parts = line.Split(':');
                var name = parts[0];
                var ranges = parts[1].Split(" or ").Select(p => p.Trim()).ToArray();
                var firstRange = ranges[0].Split('-').Select(int.Parse).ToArray();
                var secondRange = ranges[1].Split('-').Select(int.Parse).ToArray();
                var rule = new Rule((firstRange[0], firstRange[1]), (secondRange[0], secondRange[1]));

                fields[name] = rule;
            }

            throw new InvalidOperationException("Invalid input");
        }

        private int[] ParseYourTicket(IEnumerable<string> input)
        {
            var isYourTicket = false;
            foreach (var line in input)
            {
                if (isYourTicket)
                {
                    var ticket = line.Split(',').Select(int.Parse).ToArray();
                    return ticket;
                }

                if (line == "your ticket:")
                {
                    isYourTicket = true;
                }
            }

            throw new InvalidOperationException("Invalid input");
        }

        private IReadOnlyCollection<int[]> ParseOtherTickets(IEnumerable<string> input)
        {
            var tickets = new List<int[]>();
            var isNearbyTickets = false;
            foreach (var line in input)
            {
                if (isNearbyTickets)
                {
                    var ticket = line.Split(',').Select(int.Parse).ToArray();
                    tickets.Add(ticket);
                }

                if (line == "nearby tickets:")
                {
                    isNearbyTickets = true;
                }
            }

            return tickets;
        }
    }
}
