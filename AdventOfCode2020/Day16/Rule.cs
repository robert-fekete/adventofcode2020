﻿using System;

namespace AdventOfCode2020.Day16
{
    internal class Rule
    {
        private readonly (int From, int To) rangeA;
        private readonly (int From, int To) rangeB;

        public Rule((int, int) rangeA, (int, int) rangeB)
        {
            this.rangeA = rangeA;
            this.rangeB = rangeB;
        }

        internal bool IsValid(int field)
        {
            return (rangeA.From <= field && field <= rangeA.To) || (rangeB.From <= field && field <= rangeB.To);
        }
    }
}