﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020.Day22
{
    class Solver : ISolver
    {
        public int DayNumber => 22;

        public string FirstExpected => "30197";

        public string SecondExpected => "34031";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "Player 1:",
                    "9",
                    "2",
                    "6",
                    "3",
                    "1",
                    "",
                    "Player 2:",
                    "5",
                    "8",
                    "4",
                    "7",
                    "10",
                }, "306")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "Player 1:",
                    "9",
                    "2",
                    "6",
                    "3",
                    "1",
                    "",
                    "Player 2:",
                    "5",
                    "8",
                    "4",
                    "7",
                    "10",
                }, "291")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            (var deck1, var deck2) = ParseDecks(input);
            while (deck1.Any() && deck2.Any())
            {
                var card1 = deck1.Dequeue();
                var card2 = deck2.Dequeue();

                if (card1 > card2)
                {
                    deck1.Enqueue(card1);
                    deck1.Enqueue(card2);
                }
                else
                {
                    deck2.Enqueue(card2);
                    deck2.Enqueue(card1);
                }
            }

            var winnerDeck = deck1.Any() ? deck1 : deck2;

            var score = CalculateScore(winnerDeck);

            return score.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            (var deck1, var deck2) = ParseDecks(input);

            (var _, var winnerDeck) = RecursiveCombat(deck1, deck2);

            var score = CalculateScore(winnerDeck);

            return score.ToString();
        }

        public (int, IEnumerable<int>) RecursiveCombat(IEnumerable<int> cards1, IEnumerable<int> cards2)
        {
            var deck1 = new Queue<int>(cards1);
            var deck2 = new Queue<int>(cards2);

            var visited1 = new HashSet<string>();
            var visited2 = new HashSet<string>();
            while (deck1.Any() && deck2.Any())
            {
                var hash1 = string.Join(",", deck1);
                var hash2 = string.Join(",", deck2);

                if (visited1.Contains(hash1) || visited2.Contains(hash2))
                {
                    return (1, Enumerable.Empty<int>());
                }
                visited1.Add(hash1);
                visited2.Add(hash2);

                var card1 = deck1.Dequeue();
                var card2 = deck2.Dequeue();

                int winner;
                if (card1 <= deck1.Count && card2 <= deck2.Count)
                {
                    winner = RecursiveCombat(deck1.Take(card1), deck2.Take(card2)).Item1;
                }
                else
                {
                    winner = card1 > card2 ? 1 : 2;
                }

                if (winner == 1)
                {
                    deck1.Enqueue(card1);
                    deck1.Enqueue(card2);
                }
                else if (winner == 2)
                {
                    deck2.Enqueue(card2);
                    deck2.Enqueue(card1);
                }
                else
                {
                    throw new InvalidOperationException($"Invalid winner {winner}");
                }
            }

            if (deck1.Any())
            {
                return (1, deck1);
            }
            else
            {
                return (2, deck2);
            }
        }

        private (Queue<int>, Queue<int>) ParseDecks(IEnumerable<string> input)
        {
            var decks = new[] { new List<int>(), new List<int>() };
            var deckIndex = 0;
            foreach(var line in input)
            {
                if (line == string.Empty) 
                {
                    deckIndex++;
                }
                else if (!line.StartsWith("Player"))
                {
                    var value = int.Parse(line);
                    decks[deckIndex].Add(value);
                }
            }

            return (new Queue<int>(decks[0]), new Queue<int>(decks[1]));
        }

        private int CalculateScore(IEnumerable<int> winnerDeck)
        {
            var index = 1;
            var score = 0;
            foreach (var card in winnerDeck.Reverse())
            {
                score += index * card;
                index++;
            }

            return score;
        }
    }
}
