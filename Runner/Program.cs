﻿using AdventOfCode2020;
using System;

namespace Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            var runner = new RunnerFactory().Create();

            runner.Run(DateTime.Now.Day);
        }
    }
}
